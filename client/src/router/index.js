import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/pages/Landing'
import TarumanegaraHome from '@/pages/bumiyasa/Home'
import TarumanegaraAbout from '@/pages/bumiyasa/About'
import TarumanegaraProjects from '@/pages/bumiyasa/Projects'
import TarumanegaraProjectDetail from '@/pages/bumiyasa/ProjectDetail'
import TarumanegaraServices from '@/pages/bumiyasa/Services'
import TarumanegaraService from '@/pages/bumiyasa/Service'
import TarumanegaraServiceSubs from '@/pages/bumiyasa/Servicesubs'
import TarumanegaraClients from '@/pages/bumiyasa/Clients'
import TarumanegaraTeam from '@/pages/bumiyasa/Team'
import TarumanegaraNews from '@/pages/bumiyasa/News'
import TarumanegaraNewsDetail from '@/pages/bumiyasa/Newsdetail'
import TarumanegaraContact from '@/pages/bumiyasa/Contact'
import BuanatekHome from '@/pages/buanatek/Home'
import BuanatekAbout from '@/pages/buanatek/About'
import BuanatekProjects from '@/pages/buanatek/Projects'
import BuanatekProjectDetail from '@/pages/buanatek/ProjectDetail'
import BuanatekServices from '@/pages/buanatek/Services'
import BuanatekClients from '@/pages/buanatek/Clients'
import BuanatekTeam from '@/pages/buanatek/Team'
import BuanatekNews from '@/pages/buanatek/News'
import BuanatekContact from '@/pages/buanatek/Contact'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  base: "/",
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
      meta: {
        title: 'Tarumanegara',
        bodyclass: 'landing'
      }
    },
    {
      path: '/bumiyasa/home',
      name: 'TarumanegaraHome',
      component: TarumanegaraHome,
      meta: {
        title: 'Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/about',
      name: 'TarumanegaraAbout',
      component: TarumanegaraAbout,
      meta: {
        title: 'About Us - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/projects',
      name: 'TarumanegaraProjects',
      component: TarumanegaraProjects,
      meta: {
        title: 'Our Projects - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/project/:slug',
      name: 'TarumanegaraProjectDetail',
      component: TarumanegaraProjectDetail,
      meta: {
        title: 'Our Projects - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/services',
      name: 'TarumanegaraServices',
      component: TarumanegaraServices,
      meta: {
        title: 'Our Services - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },{
      path: '/bumiyasa/service/:slug',
      name: 'TarumanegaraService',
      component: TarumanegaraService,
      meta: {
        title: 'Service - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },{
      path: '/bumiyasa/servicesubs/:slug',
      name: 'TarumanegaraServiceSubs',
      component: TarumanegaraServiceSubs,
      meta: {
        title: 'Service Subs - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },{
      path: '/bumiyasa/client',
      name: 'TarumanegaraClients',
      component: TarumanegaraClients,
      meta: {
        title: 'Our Clients - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/team',
      name: 'TarumanegaraTeam',
      component: TarumanegaraTeam,
      meta: {
        title: 'Team Support - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/news',
      name: 'TarumanegaraNews',
      component: TarumanegaraNews,
      meta: {
        title: 'News - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/news/:slug',
      name: 'TarumanegaraNewsDetail',
      component: TarumanegaraNewsDetail,
      meta: {
        title: 'News - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },
    {
      path: '/bumiyasa/contact',
      name: 'TarumanegaraContact',
      component: TarumanegaraContact,
      meta: {
        title: 'Contact Us - Tarumanegara Bumiyasa',
        bodyclass: 'tarumanegara'
      }
    },

    {
      path: '/buanatek/home',
      name: 'BuanatekHome',
      component: BuanatekHome,
      meta: {
        title: 'Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/about',
      name: 'BuanatekAbout',
      component: BuanatekAbout,
      meta: {
        title: 'About Us',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/projects',
      name: 'BuanatekProjects',
      component: BuanatekProjects,
      meta: {
        title: 'Our Projects - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/project/:slug',
      name: 'BuanatekProjectDetail',
      component: BuanatekProjectDetail,
      meta: {
        title: 'Our Projects - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/services',
      name: 'BuanatekServices',
      component: BuanatekServices,
      meta: {
        title: 'Our Services - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/client',
      name: 'BuanatekClients',
      component: BuanatekClients,
      meta: {
        title: 'Our Clients - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/team',
      name: 'BuanatekTeam',
      component: BuanatekTeam,
      meta: {
        title: 'Team Support - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/news',
      name: 'BuanatekNews',
      component: BuanatekNews,
      meta: {
        title: 'News - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    },
    {
      path: '/buanatek/contact',
      name: 'BuanatekContact',
      component: BuanatekContact,
      meta: {
        title: 'Contact Us - Tarumanegara Buanatek',
        bodyclass: 'buanatek'
      }
    }
  ]
})
