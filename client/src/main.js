// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import NavigationBumiyasa from "@/components/NavigationBumiyasa";
import NavigationBuanatek from "@/components/NavigationBuanatek";

Vue.component('navigation-bumiyasa', NavigationBumiyasa);
Vue.component('navigation-buanatek', NavigationBuanatek);

Vue.prototype.$baseUrl = process.env.NODE_ENV === "development" ? "http://taruma.test" : "http://taruma.ekode.xyz";

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
