var $ = require('jquery');
window.$ = $;
require('bootstrap');

if (module.hot) {
  module.hot.accept()
}

const hamburger = $('#hamburger');

if(hamburger) {
  hamburger.on('click', e => {
    e.preventDefault();

    if (hamburger.hasClass('is-active')) {
      hamburger.removeClass('is-active')
      hamburger.attr('aria-expanded', 'false')
      $('#navigation').removeClass('enter')
    }
    else {
      hamburger.addClass('is-active')
      hamburger.attr('aria-expanded', 'true')
      $('#navigation').addClass('enter')
    }
  })
}