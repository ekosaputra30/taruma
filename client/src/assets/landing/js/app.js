import Glide from '@glidejs/glide'

if (module.hot) {
  module.hot.accept()
}

const glide = new Glide('.glide', {
  type: 'carousel',
  startAt: 0,
  perView: 1,
  breakpoints: {
    768: {
      perView: 1
    }
  }
})

glide.mount()

console.log('Hello')
