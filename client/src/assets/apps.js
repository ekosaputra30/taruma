function navigation() {
  const hamburger = document.getElementById('hamburger')
  const navigation = document.getElementById('navigation')

  hamburger.onclick = () => {
    console.log('burger clicked')
    let clsName = 'is-active'
    let clsEnter = 'enter'
    let isActive = hamburger.classList.contains(clsName)

    if (isActive) {
      hamburger.classList.remove(clsName)
      navigation.classList.remove(clsEnter)
    } else {
      hamburger.classList.add(clsName)
      navigation.classList.add(clsEnter)
    }
  }
}

function bodyClass(cls) {
  document.body.setAttribute('class', '')
  document.body.classList.add(cls)
}

export { bodyClass, navigation }