import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    locale: 'EN' // The TV inventory
  },
  
  getters: {
    activeLocale: state => {
      return state.locale
    }
  },
  
  mutations: {
    changeLocale(state, newLocale) {
      state.locale = newLocale
    }
  },
  
  actions: {
    changeLocale(context, newLocale) {
      context.commit('changeLocale', newLocale)
    }
  }
});