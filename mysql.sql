-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jul 29, 2020 at 01:26 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tarumanegara`
--
CREATE DATABASE IF NOT EXISTS `tarumanegara` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tarumanegara`;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int NOT NULL,
  `logo` varchar(255) NOT NULL,
  `company` varchar(200) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `status` tinyint NOT NULL,
  `fax` varchar(20) NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `logo`, `company`, `name`, `slug`, `telp`, `address`, `status`, `fax`, `extra`) VALUES
(1, 'bumiyasa.jpg', 'Tarumanegara Bumiyasa', 'Bumiyasa', 'bumiyasa', '', '', 1, '', ''),
(2, 'buanatek.jpg', 'Tarumanegara Buanatek', 'Buanatek', 'buanatek', '', '', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int NOT NULL,
  `language` varchar(50) DEFAULT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `is_default` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`, `slug`, `code`, `image`, `is_default`, `is_active`) VALUES
(1, 'Indonesia', 'indonesia', 'ID', 'flag-id.png', 1, 1),
(2, 'English', 'english', 'EN', 'flag-en.png', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(17, '172.19.0.1', 'admin', 1595566482),
(18, '172.19.0.1', 'admin@cms.id', 1595566525);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$GL0LDD48ZTE8FdcUEJVpAe5ufeMwv.Nof2RiA./3I9/.H0OBl1Tzy', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1595566627, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
--
-- Database: `tarumanegara_buanatek`
--
CREATE DATABASE IF NOT EXISTS `tarumanegara_buanatek` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tarumanegara_buanatek`;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int NOT NULL,
  `category_id` int NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `category_id`, `name`, `slug`, `url`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'PT. Total Bangun Persada', 'pt-total-bangun-persada', 'http://www.totalbp.com/index/en', 'total.jpg', 1, '2020-04-22 22:20:11', '2020-04-22 22:26:49'),
(2, 2, 'PT. Pakubumi Semesta', 'pt-pakubumi-semesta', 'http://sispro.co.id/pt-pakubumi-semesta-152.htm', 'pakubumi1.jpg', 1, '2020-04-22 22:55:43', '2020-04-22 23:12:29'),
(3, 3, 'PT. Tempo Scan Pacific TBK', 'pt-tempo-scan-pacific-tbk', 'http://www.thetempogroup.net/', 'tempo.jpg', 1, '2020-04-22 23:14:38', NULL),
(4, 4, 'PT Adhi Persada Properti', 'pt-adhi-persada-properti', 'http://adhipersadaproperti.co.id', 'adhi.jpg', 1, '2020-04-22 23:15:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_categories`
--

CREATE TABLE `client_categories` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_categories`
--

INSERT INTO `client_categories` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Private Owned Foundation', 'private-owned-foundation', 1, '2020-04-22 21:29:01', '2020-04-22 21:29:01'),
(2, 'Private Owned Construction', 'private-owned-construction', 1, '2020-04-22 22:27:50', NULL),
(3, 'Private Companies', 'private-companies', 1, '2020-04-22 22:28:08', NULL),
(4, 'Government Owned Companies', 'government-owned-companies', 1, '2020-04-22 22:35:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'ID',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `slug`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Pondasi Bored Pile Proyek Gedung', 'pondasi-bored-pile-proyek-gedung', '', NULL, 1, '2020-07-12 22:26:26', NULL),
(4, 'Pondasi Bored Pile Proyek Jembatan', 'pondasi-bored-pile-proyek-jembatan', '', NULL, 1, '2020-07-12 22:26:40', NULL),
(5, 'Pondasi Bored Pile Proyek Underpinning', 'pondasi-bored-pile-proyek-underpinning', '', NULL, 1, '2020-07-12 22:26:54', NULL),
(6, 'Pondasi Bored Pile Proyek Pabrik /Mesin', 'pondasi-bored-pile-proyek-pabrik-mesin', '', NULL, 1, '2020-07-12 22:27:07', NULL),
(7, 'Pondasi Bored Pile Proyek Turap', 'pondasi-bored-pile-proyek-turap', '', NULL, 1, '2020-07-12 22:27:19', NULL),
(8, 'Proyek Shotcrete & Soil Nailling', 'proyek-shotcrete-soil-nailling', '', NULL, 1, '2020-07-12 22:27:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_lists`
--

CREATE TABLE `project_lists` (
  `id` bigint NOT NULL,
  `project_id` int NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `client` varchar(255) NOT NULL,
  `location` text,
  `start` year DEFAULT NULL,
  `finish` year DEFAULT NULL,
  `basement` varchar(100) DEFAULT NULL,
  `surface` bigint NOT NULL DEFAULT '0',
  `depth` bigint NOT NULL DEFAULT '0',
  `jasa_per_m` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_lists`
--

INSERT INTO `project_lists` (`id`, `project_id`, `name`, `client`, `location`, `start`, `finish`, `basement`, `surface`, `depth`, `jasa_per_m`) VALUES
(78, 3, 'Amaris Hotel - Extension (2015)', 'Amaris Hotel - Extension (2015)', 'Juanda Jakarta', NULL, NULL, NULL, 0, 0, NULL),
(79, 3, 'Gedung Kantor Graha Cikini (2016', 'Gedung Kantor Graha Cikini (2016', 'Jl. Cikini Raya No.28B Jakarta Pusat', NULL, NULL, NULL, 0, 0, NULL),
(80, 3, 'Hotel Lalucia (2016)', 'Hotel Lalucia (2016)', 'Jl. Air Saga Tj Pandang Belitung', NULL, NULL, NULL, 0, 0, NULL),
(81, 4, 'Perkuatan Jembatan Pedestrian (2016', 'Perkuatan Jembatan Pedestrian (2016', 'Tanah Abang Jakarta Pusat', NULL, NULL, NULL, 0, 0, NULL),
(82, 4, 'JPO Grand Metropolitan Mall - M Gold (2017)', 'JPO Grand Metropolitan Mall - M Gold (2017)', 'Bekasi Barat', NULL, NULL, NULL, 0, 0, NULL),
(83, 4, 'Abutmen Jembatan Kali Suka Danau (2017)', 'Abutmen Jembatan Kali Suka Danau (2017)', 'Cibitung - Bekasi', NULL, NULL, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` text,
  `images` text,
  `lang` varchar(10) NOT NULL DEFAULT 'ID',
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `parent_id`, `name`, `slug`, `description`, `images`, `lang`, `status`, `created_at`, `updated_at`) VALUES
(7, 7, 'Bored Pile Metode Wash Boring', 'bored-pile-metode-wash-boring', 'Bored Pile Metode Wash Boring', 'a:10:{i:0;a:5:{s:4:\"name\";s:12:\"100_4020.JPG\";s:12:\"originalName\";s:12:\"100_4020.JPG\";s:4:\"size\";i:2396410;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"753ed1f0-b51d-47d6-8f80-bb2f145dca48\";}i:1;a:5:{s:4:\"name\";s:12:\"100_5517.JPG\";s:12:\"originalName\";s:12:\"100_5517.JPG\";s:4:\"size\";i:2475846;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"00291cab-9bf2-4322-bb97-109a6dd09d5d\";}i:2;a:5:{s:4:\"name\";s:12:\"100_5518.JPG\";s:12:\"originalName\";s:12:\"100_5518.JPG\";s:4:\"size\";i:2506129;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"46cf90ce-bdae-40c9-9f38-86cb38123b6b\";}i:3;a:5:{s:4:\"name\";s:19:\"20170522_135425.jpg\";s:12:\"originalName\";s:19:\"20170522_135425.jpg\";s:4:\"size\";i:1216274;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"4598b760-14a6-4bb8-887a-780b026ed0f8\";}i:4;a:5:{s:4:\"name\";s:12:\"Cleaning.jpg\";s:12:\"originalName\";s:12:\"Cleaning.jpg\";s:4:\"size\";i:54933;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"00df5134-a9d2-4499-ada8-f5df2d62c780\";}i:5;a:5:{s:4:\"name\";s:15:\"Pengeboran .jpg\";s:12:\"originalName\";s:15:\"Pengeboran .jpg\";s:4:\"size\";i:85018;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"d7b3b168-12b0-4b1f-921d-bb0bebaefb7c\";}i:6;a:5:{s:4:\"name\";s:13:\"service 1.jpg\";s:12:\"originalName\";s:13:\"service 1.jpg\";s:4:\"size\";i:110303;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"cdd3cff4-6855-492c-8429-69fcc7edd871\";}i:7;a:5:{s:4:\"name\";s:13:\"service 2.jpg\";s:12:\"originalName\";s:13:\"service 2.jpg\";s:4:\"size\";i:79399;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"06f28986-2dbe-4472-bee7-12c65a15620d\";}i:8;a:5:{s:4:\"name\";s:13:\"service 3.jpg\";s:12:\"originalName\";s:13:\"service 3.jpg\";s:4:\"size\";i:67388;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"47b8a22a-a652-4371-a3ce-97ea23bb4808\";}i:9;a:5:{s:4:\"name\";s:13:\"Service 4.JPG\";s:12:\"originalName\";s:13:\"Service 4.JPG\";s:4:\"size\";i:2508293;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"96ae0b18-d25a-4c2e-879f-6e7376282b7e\";}}', 'ID', 1, '2020-07-12 19:27:29', '2020-07-12 19:32:24'),
(8, 7, 'Bored Pile Metode Wash Boring', 'bored-pile-metode-wash-boring', 'Bored Pile Metode Wash Boring', 'a:10:{i:0;a:5:{s:4:\"name\";s:12:\"100_4020.JPG\";s:12:\"originalName\";s:12:\"100_4020.JPG\";s:4:\"size\";i:2396410;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"753ed1f0-b51d-47d6-8f80-bb2f145dca48\";}i:1;a:5:{s:4:\"name\";s:12:\"100_5517.JPG\";s:12:\"originalName\";s:12:\"100_5517.JPG\";s:4:\"size\";i:2475846;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"00291cab-9bf2-4322-bb97-109a6dd09d5d\";}i:2;a:5:{s:4:\"name\";s:12:\"100_5518.JPG\";s:12:\"originalName\";s:12:\"100_5518.JPG\";s:4:\"size\";i:2506129;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"46cf90ce-bdae-40c9-9f38-86cb38123b6b\";}i:3;a:5:{s:4:\"name\";s:19:\"20170522_135425.jpg\";s:12:\"originalName\";s:19:\"20170522_135425.jpg\";s:4:\"size\";i:1216274;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"4598b760-14a6-4bb8-887a-780b026ed0f8\";}i:4;a:5:{s:4:\"name\";s:12:\"Cleaning.jpg\";s:12:\"originalName\";s:12:\"Cleaning.jpg\";s:4:\"size\";i:54933;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"00df5134-a9d2-4499-ada8-f5df2d62c780\";}i:5;a:5:{s:4:\"name\";s:15:\"Pengeboran .jpg\";s:12:\"originalName\";s:15:\"Pengeboran .jpg\";s:4:\"size\";i:85018;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"d7b3b168-12b0-4b1f-921d-bb0bebaefb7c\";}i:6;a:5:{s:4:\"name\";s:13:\"service 1.jpg\";s:12:\"originalName\";s:13:\"service 1.jpg\";s:4:\"size\";i:110303;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"cdd3cff4-6855-492c-8429-69fcc7edd871\";}i:7;a:5:{s:4:\"name\";s:13:\"service 2.jpg\";s:12:\"originalName\";s:13:\"service 2.jpg\";s:4:\"size\";i:79399;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"06f28986-2dbe-4472-bee7-12c65a15620d\";}i:8;a:5:{s:4:\"name\";s:13:\"service 3.jpg\";s:12:\"originalName\";s:13:\"service 3.jpg\";s:4:\"size\";i:67388;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"47b8a22a-a652-4371-a3ce-97ea23bb4808\";}i:9;a:5:{s:4:\"name\";s:13:\"Service 4.JPG\";s:12:\"originalName\";s:13:\"Service 4.JPG\";s:4:\"size\";i:2508293;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"96ae0b18-d25a-4c2e-879f-6e7376282b7e\";}}', 'EN', 1, '2020-07-12 19:27:29', '2020-07-12 19:32:24'),
(9, 9, 'Coring', 'coring', 'Coring', 'a:19:{i:0;a:5:{s:4:\"name\";s:10:\"Coring.jpg\";s:12:\"originalName\";s:10:\"Coring.jpg\";s:4:\"size\";i:276727;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"47c243ed-d94e-445e-86d2-8ba41121d646\";}i:1;a:5:{s:4:\"name\";s:11:\"coring2.jpg\";s:12:\"originalName\";s:11:\"coring2.jpg\";s:4:\"size\";i:137159;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a9c46071-85d0-4685-99b9-5a25d203ab06\";}i:2;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0036.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0036.jpg\";s:4:\"size\";i:199894;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"44aef868-6a6c-4690-ba0f-bf6934c0d2c2\";}i:3;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0038.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0038.jpg\";s:4:\"size\";i:170336;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"b03a48a1-50c5-46ad-8a5f-d28df76fcddc\";}i:4;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0040.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0040.jpg\";s:4:\"size\";i:191363;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"fea654b9-4f30-42f3-ad2d-376b77a14bbc\";}i:5;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0042.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0042.jpg\";s:4:\"size\";i:198432;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"d9c1e673-6b8b-47e4-81ed-aea9f8cedbf3\";}i:6;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0049.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0049.jpg\";s:4:\"size\";i:188983;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"1077140a-d055-4f1e-8b91-4e844a4eab59\";}i:7;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0053.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0053.jpg\";s:4:\"size\";i:228916;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3f29a979-c9bd-46c3-a0fa-924f869c8a30\";}i:8;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0054.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0054.jpg\";s:4:\"size\";i:164463;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"f870c0ad-0dcd-4f02-9262-730a7978ee55\";}i:9;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0056.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0056.jpg\";s:4:\"size\";i:227411;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"bcde7d35-8742-4a55-b977-a31b55cffe50\";}i:10;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0064.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0064.jpg\";s:4:\"size\";i:199894;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"46245ed7-bc89-4960-b37f-49af5083cbcc\";}i:11;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0068.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0068.jpg\";s:4:\"size\";i:285431;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"e93d07a4-2f74-4866-b840-55b7a12143fa\";}i:12;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0069.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0069.jpg\";s:4:\"size\";i:281365;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a4c81873-264f-432b-83ef-316c635ae6fb\";}i:13;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0072.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0072.jpg\";s:4:\"size\";i:296957;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"e213f37f-b502-45e9-a5a7-891ebf1cbab7\";}i:14;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0075.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0075.jpg\";s:4:\"size\";i:233753;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3a207b41-2785-4522-9941-b6f2e715f02b\";}i:15;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0083.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0083.jpg\";s:4:\"size\";i:285431;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"1db6f9fa-09ea-4cd5-b0b5-d6bea898fbf6\";}i:16;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0084.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0084.jpg\";s:4:\"size\";i:281365;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"82e5dd00-76cf-4a19-89dc-48805b59d19f\";}i:17;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0088.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0088.jpg\";s:4:\"size\";i:296957;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3a7e9421-8338-487d-8d47-8a0ba7ec54c3\";}i:18;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0092.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0092.jpg\";s:4:\"size\";i:233753;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a6980318-ccfc-4152-b1b2-fa4c12cf185f\";}}', 'ID', 1, '2020-07-12 21:37:31', '2020-07-12 22:04:01'),
(10, 9, 'Coring', 'coring', 'Coring', 'a:19:{i:0;a:5:{s:4:\"name\";s:10:\"Coring.jpg\";s:12:\"originalName\";s:10:\"Coring.jpg\";s:4:\"size\";i:276727;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"47c243ed-d94e-445e-86d2-8ba41121d646\";}i:1;a:5:{s:4:\"name\";s:11:\"coring2.jpg\";s:12:\"originalName\";s:11:\"coring2.jpg\";s:4:\"size\";i:137159;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a9c46071-85d0-4685-99b9-5a25d203ab06\";}i:2;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0036.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0036.jpg\";s:4:\"size\";i:199894;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"44aef868-6a6c-4690-ba0f-bf6934c0d2c2\";}i:3;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0038.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0038.jpg\";s:4:\"size\";i:170336;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"b03a48a1-50c5-46ad-8a5f-d28df76fcddc\";}i:4;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0040.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0040.jpg\";s:4:\"size\";i:191363;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"fea654b9-4f30-42f3-ad2d-376b77a14bbc\";}i:5;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0042.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0042.jpg\";s:4:\"size\";i:198432;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"d9c1e673-6b8b-47e4-81ed-aea9f8cedbf3\";}i:6;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0049.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0049.jpg\";s:4:\"size\";i:188983;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"1077140a-d055-4f1e-8b91-4e844a4eab59\";}i:7;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0053.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0053.jpg\";s:4:\"size\";i:228916;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3f29a979-c9bd-46c3-a0fa-924f869c8a30\";}i:8;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0054.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0054.jpg\";s:4:\"size\";i:164463;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"f870c0ad-0dcd-4f02-9262-730a7978ee55\";}i:9;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0056.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0056.jpg\";s:4:\"size\";i:227411;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"bcde7d35-8742-4a55-b977-a31b55cffe50\";}i:10;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0064.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0064.jpg\";s:4:\"size\";i:199894;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"46245ed7-bc89-4960-b37f-49af5083cbcc\";}i:11;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0068.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0068.jpg\";s:4:\"size\";i:285431;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"e93d07a4-2f74-4866-b840-55b7a12143fa\";}i:12;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0069.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0069.jpg\";s:4:\"size\";i:281365;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a4c81873-264f-432b-83ef-316c635ae6fb\";}i:13;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0072.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0072.jpg\";s:4:\"size\";i:296957;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"e213f37f-b502-45e9-a5a7-891ebf1cbab7\";}i:14;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0075.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0075.jpg\";s:4:\"size\";i:233753;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3a207b41-2785-4522-9941-b6f2e715f02b\";}i:15;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0083.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0083.jpg\";s:4:\"size\";i:285431;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"1db6f9fa-09ea-4cd5-b0b5-d6bea898fbf6\";}i:16;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0084.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0084.jpg\";s:4:\"size\";i:281365;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"82e5dd00-76cf-4a19-89dc-48805b59d19f\";}i:17;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0088.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0088.jpg\";s:4:\"size\";i:296957;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"3a7e9421-8338-487d-8d47-8a0ba7ec54c3\";}i:18;a:5:{s:4:\"name\";s:23:\"IMG-20171102-WA0092.jpg\";s:12:\"originalName\";s:23:\"IMG-20171102-WA0092.jpg\";s:4:\"size\";i:233753;s:7:\"batchid\";N;s:4:\"uuid\";s:36:\"a6980318-ccfc-4152-b1b2-fa4c12cf185f\";}}', 'EN', 1, '2020-07-12 21:37:32', '2020-07-12 22:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `service_subs`
--

CREATE TABLE `service_subs` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `service_id` int DEFAULT NULL,
  `lang` varchar(10) NOT NULL DEFAULT 'ID',
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` text,
  `images` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `telp` text,
  `address` text,
  `fax` text,
  `extra` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `telp`, `address`, `fax`, `extra`) VALUES
(2, 'logo-buanatek.jpeg', 'tel: (021)15321547<br>\r\ntel: (021)5308689<br>\r\ntel: (021)5308690<br>\r\ntel: (021)5481413', 'jl. Raya Kebayoran Lama No.354<br>\r\nSukabumi Utara, Kebon Jeruk<br>\r\nJakarta Barat 11540', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `division_id` int NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `educatons` text,
  `certifications` text,
  `role` varchar(255) DEFAULT NULL,
  `join_year` year DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `division_id`, `image`, `educatons`, `certifications`, `role`, `join_year`, `created_at`, `updated_at`) VALUES
(15, 'Agus Johan', 4, 'Agus Johan (Direktur Utama).jpg', NULL, NULL, 'Direktur Utama', NULL, '2020-07-12 22:20:44', NULL),
(16, 'Budiman', 5, 'Budiman (Operasional).jpeg', NULL, NULL, 'Operasional', NULL, '2020-07-12 22:21:16', NULL),
(17, 'Febri Yulianti', 5, 'Febri Yulianti (Marketing).jpeg', NULL, NULL, 'Marketing', NULL, '2020-07-12 22:21:53', NULL),
(18, 'Jheval S.E', 5, 'Jheval S E (Manager Operasional).jpeg', NULL, NULL, 'Manager Operasional', NULL, '2020-07-12 22:22:18', NULL),
(19, 'Muhtar SBU', 5, 'Muhtar SBU (Kepala Div Maintenance  Peralatan ).JPG', NULL, NULL, 'Kepala Div Maintenance & Peralatan', NULL, '2020-07-12 22:22:47', NULL),
(20, 'Mukiman', 5, 'Mukiman (Kelapa div Sedot Lumpur).JPG', NULL, NULL, 'Kelapa div Sedot Lumpur', NULL, '2020-07-12 22:23:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_divisions`
--

CREATE TABLE `team_divisions` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_divisions`
--

INSERT INTO `team_divisions` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Board of Directors', 'board-of-directors', 1, '2020-07-12 22:19:16', NULL),
(5, 'Manager & Head of Division', 'manager-head-of-division', 1, '2020-07-12 22:19:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_categories`
--
ALTER TABLE `client_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_lists`
--
ALTER TABLE `project_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_subs`
--
ALTER TABLE `service_subs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_divisions`
--
ALTER TABLE `team_divisions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `client_categories`
--
ALTER TABLE `client_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project_lists`
--
ALTER TABLE `project_lists`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `service_subs`
--
ALTER TABLE `service_subs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `team_divisions`
--
ALTER TABLE `team_divisions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Database: `tarumanegara_bumiyasa`
--
CREATE DATABASE IF NOT EXISTS `tarumanegara_bumiyasa` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tarumanegara_bumiyasa`;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int NOT NULL,
  `category_id` int NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `category_id`, `name`, `slug`, `url`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'PT. Total Bangun Persada', 'pt-total-bangun-persada', 'http://www.totalbp.com/index/en', 'total.jpg', 1, '2020-04-22 22:20:11', '2020-04-22 22:26:49'),
(2, 2, 'PT. Pakubumi Semesta', 'pt-pakubumi-semesta', 'http://sispro.co.id/pt-pakubumi-semesta-152.htm', 'pakubumi1.jpg', 1, '2020-04-22 22:55:43', '2020-04-22 23:12:29'),
(3, 3, 'PT. Tempo Scan Pacific TBK', 'pt-tempo-scan-pacific-tbk', 'http://www.thetempogroup.net/', 'tempo.jpg', 1, '2020-04-22 23:14:38', NULL),
(4, 4, 'PT Adhi Persada Properti', 'pt-adhi-persada-properti', 'http://adhipersadaproperti.co.id', 'adhi.jpg', 1, '2020-04-22 23:15:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_categories`
--

CREATE TABLE `client_categories` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_categories`
--

INSERT INTO `client_categories` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Private Owned Foundation', 'private-owned-foundation', 1, '2020-04-22 21:29:01', '2020-04-22 21:29:01'),
(2, 'Private Owned Construction', 'private-owned-construction', 1, '2020-04-22 22:27:50', NULL),
(3, 'Private Companies', 'private-companies', 1, '2020-04-22 22:28:08', NULL),
(4, 'Government Owned Companies', 'government-owned-companies', 1, '2020-04-22 22:35:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'ID',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `parent_id`, `lang`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`, `status`) VALUES
(3, NULL, 'ID', 'Wash Boring (Direct Circulation) Method', 'wash-boring-direct-circulation-method', '<p><strong>Wash boring atau bor bilas (Direct Circulation) merupakan proses pemboran dengan diameter dan kedalaman yang telah direncanakan, menggunakan putaran / rotary sekaligus memberi tekanan pada mata bor dan menyemprotkan fluida berupa air atau lumpur yang disirkulasi dengan pompa lumpur bertekanan melalui pipa bor dan tanah hasil pemboran (cutting) akan terangkat ke permukaan diantara dinding lubang dan pipa bor karena daya angkat sirkulasi fluida. Selain itu, sirkulasi fluida juga berfungsi membersihkan lubang bor guna memperkecil viskositas lumpur untuk memudahkan pembersihan lanjutan secara mekanik menggunakan BAILER.</strong></p><p>&nbsp;</p><p><strong>Proses Pemboran / Wash Boring</strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Proses Pembersihan Lubang Bor</strong></p><p>&nbsp;</p><p>&nbsp;</p><p><br />&nbsp;</p><ol><li><ol><li><p><strong>Kelebihan Tiang Bor Rotary Wash Boring YBM</strong></p><ol><li><p><strong>Peralatan cukup ringkas dan ringan, namun mampu menghasilkan tiang bor diameter 300 &ndash; 1000 mm dengan kedalaman 40 &ndash; 50 m dalam waktu 1 x 24 jam.</strong></p></li><li><p><strong>Perubahan geometeri (dimensi) tiang bor mudah dilakukan baik diameter maupun kedalaman manakala lapisan tanah bor menentukan keharusan penyesuaian.</strong></p></li><li><p><strong>Pembengkakan dan penurunan permukaan tanah sekeliling sangat kecil mendekati normal.</strong></p></li><li><p><strong>Gangguan suara dan getaran alat sangat minim dan dapat ditoleransi.</strong></p></li><li><p><strong>Ketersediaan tenaga terampil, set boring YBM, &amp; material tiang bor mudah diperoleh.</strong></p></li><li><p><strong>Pembuangan lumpur bor hasil penghancuran tanah dengan air mudah dilakukan dengan mobil tangki cakum.</strong></p></li><li><p><strong>Kemungkinan tiang tunggal karena kapasitas besar.</strong></p></li><li><p><strong>Sangat efektif digunakan untuk pekerjaan UNDERPINNING.</strong></p></li><li><p><strong>Verfikasi Integritas dan kapasitas tiang bor dapat dilakukan dengan mudah setelah tiang bor umur &gt; 14 hari memanfaatkan teknologi PIT dan DLT/PDA plus drop hummer yang kami miliki.</strong></p></li></ol></li></ol></li></ol><p>&nbsp;</p><ol><li><ol><li><p><strong>Keterbatasan Tiang Bor Rotary Wash Boring YBM</strong></p><ol><li><p><strong>Pemboran efektif pada lapisan tanah alluvial / endapan berputir halus dengan N-SPT = 60 (bukan baunching) dan bukan endapan glacial berupa pasir kasar, kerikil apalagi kerakal.</strong></p></li><li><p><strong>Ketersediaan air tanah/sumber air untuk proses pemboran wash boring biaya meningkat jika haus membeli air kerja.</strong></p></li><li><p><strong>Proses pelubangan tiang; pembersihan lumpur; pemasangan besi dan tremie cor; menunggu pasokan beton atau aduk sendiri, pengecoran merupakan proses panjang setara 1x24 jam untuk menghasilkan 1 tiang.</strong></p></li><li><p><strong>Proses panjang tiang bor membutuhkan supervisi dan inspeksi melekat oleh tenaga terlatih untuk memastikan kualitas tiang bor; QA dan QC adalah suatu keharusan.</strong></p></li><li><p><strong>Kesalahan dan atau turunnya mutu tiang maka konskuensi penggantian adalah 2 tiang simetris untuk 1 tiang rusak.</strong></p></li></ol></li></ol></li></ol><p>&nbsp;</p>', '1594579545_wash-boring-direct-circulation-method.jpg', '2020-07-12 18:45:45', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `slug`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Project Soil', 'project-soil', '', 'project-soil.png', 1, '2020-04-21 20:32:36', '2020-04-21 20:42:28'),
(2, 'NDT Test Project List 2019', 'ndt-test-project-list-2019', '', 'project-ndt.png', 1, '2020-04-22 10:32:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_lists`
--

CREATE TABLE `project_lists` (
  `id` bigint NOT NULL,
  `project_id` int NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `client` varchar(255) NOT NULL,
  `location` text,
  `start` year DEFAULT NULL,
  `finish` year DEFAULT NULL,
  `basement` varchar(100) DEFAULT NULL,
  `surface` bigint NOT NULL DEFAULT '0',
  `depth` bigint NOT NULL DEFAULT '0',
  `jasa_per_m` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_lists`
--

INSERT INTO `project_lists` (`id`, `project_id`, `name`, `client`, `location`, `start`, `finish`, `basement`, `surface`, `depth`, `jasa_per_m`) VALUES
(1, 1, 'Synthesis Tower', 'PT. Sintesis Kreasi Utama', 'Jl. Gatot Subroto Kav. 17A, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(2, 1, 'Synthesis Residence', 'PT. Sintesis Kreasi Bersama', 'Jl. Ampera, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(3, 1, 'Apartemen', 'PT. Makna Alam Sejahtera', 'Jl. Alam Sutera Boulevard Kav. 16 A-B, Tangerang', NULL, NULL, NULL, 0, 0, NULL),
(4, 1, 'Hotel dan Kantor', 'PT. RHB MVP Classic Development', 'Mega Kuningan, Jakarta', NULL, NULL, NULL, 0, 0, NULL),
(5, 1, 'Prajawangsa City', 'PT. Synthesis Karya Pratama', 'Jl. Eka Dharma, Cijantung - Jakarta Timur', NULL, NULL, NULL, 0, 0, NULL),
(6, 1, 'Synthesis Residence Tambahan', 'PT. Sintesis Kreasi Bersama', 'Jl. Ampera, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(7, 1, 'IBIS Budget Hotel', 'PT. Waskita Multi Selaras', 'Jl. Jaksa, Menteng - Jakarta Pusat', NULL, NULL, NULL, 0, 0, NULL),
(8, 1, 'Grand Cipulir', 'PT. Pandhega Shora', 'Cipulir, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(9, 1, 'The Ginza Apartment', 'MGM Land', 'Bintaro, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(10, 1, 'Pabrik Kelapa Sawit', 'PT. Karya Pratama Mandiri', 'Kec. Ketungau Hilir Kab. Sintang, Kalimantan Barat', NULL, NULL, NULL, 0, 0, NULL),
(11, 1, 'Data Center 2', 'PT. Jaya Kusuma Sarana', 'Sentul, Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(12, 1, 'Penthouse Kemang', 'PT. Mahardika Gagas Sejahtera', 'Jl. Pangeran Antasari, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(13, 1, 'Pabrik Kelapa Sawit', 'PT. Karya Pratama Mandiri', 'Desa Merapun, Kec. Helai Kab. Berau - Kalimantan Timur', NULL, NULL, NULL, 0, 0, NULL),
(14, 1, 'Asiana 1 Tower', 'KSO. Waskita Asiana Senopati', 'Senopati, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(15, 1, 'Stadium Hotel dan Apartemen', 'PT. Prima Nusa Grahacitra', 'Cikarang', NULL, NULL, NULL, 0, 0, NULL),
(16, 1, 'Rumah Susun Wisma Atlet', 'Abipraya - Indolexo KSO', 'Kemayoran, Jakarta Pusat', NULL, NULL, NULL, 0, 0, NULL),
(17, 1, 'Apartemen', 'PT. China Harbour Indonesia', 'Daan Mogot', NULL, NULL, NULL, 0, 0, NULL),
(18, 1, 'The Smith', 'PT. Triniti Dinamik', 'Alam Sutera, Tangerang Selatan', NULL, NULL, NULL, 0, 0, NULL),
(19, 1, 'Dapenbi Office', 'PT. Anggara Architeam', 'Jl. Prof. Soepomo, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(20, 1, 'LRT Velodrome - Kelapa Gading', 'PT. Wijaya Karya Persero Tbk', 'Jakarta', NULL, NULL, NULL, 0, 0, NULL),
(21, 1, 'Office Puri Orchard Extention', 'PT. Adhicipta Graha Kencana', 'Jakarta Barat', NULL, NULL, NULL, 0, 0, NULL),
(22, 1, 'Hotel Double Tree - Puri Orchard', 'PT. Serenity Cipta Kencana', 'Cengkareng, Jakarta Barat', NULL, NULL, NULL, 0, 0, NULL),
(23, 1, 'PT. Bintang Toedjoe dan PT. Saka Farma', 'PT. Pembangunan Deltamas', 'Kawasan Industri GIIC, Deltamas Blok BB/6-BB/7 - Cikarang Pusat', NULL, NULL, NULL, 0, 0, NULL),
(24, 1, 'Laboratorium Tissue Culture', 'PT. Smart', 'Bogor, Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(25, 1, 'Carrefour', 'PT. Trans Retail Indonesia', 'Surabaya, Jawa Timur', NULL, NULL, NULL, 0, 0, NULL),
(26, 1, 'Pabrik', 'PT. ZTT Cable Indonesia', 'Lot 66G1&2, Jl. Surya Madya VII - Karawang', NULL, NULL, NULL, 0, 0, NULL),
(27, 1, 'Silo', 'PT. Harim Farmsco Indonesia', 'Desa Murnisari Kec. Mande Kab. Cianjur, Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(28, 1, 'Production Plant', 'PT. Prima Irian Djaja', 'Sorong, Papua', NULL, NULL, NULL, 0, 0, NULL),
(29, 1, 'PBTR Project', 'PT. Geotekindo', 'Pekalongan - Jawa Tengah', NULL, NULL, NULL, 0, 0, NULL),
(30, 1, 'Bulking Station', 'PT. Sarana Pembangunan Persada Mandiri', 'Labanan Kec. Teluk Bayur Kab. Berau - Kalimantan Timur', NULL, NULL, NULL, 0, 0, NULL),
(31, 1, 'Mandaya Royal Hospital', 'PT. Mandaya Sehat Utama', 'Metland Boulevard Kav. C-3 Metland Cyber City, Karang Tengah', NULL, NULL, NULL, 0, 0, NULL),
(32, 1, 'Apartemen Louvin', 'PT. PP Property Tbk', 'Jatinangor, Sumedang', NULL, NULL, NULL, 0, 0, NULL),
(33, 1, 'Apartemen La Montana', 'PT. Emesen Properti', 'Bogor, Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(34, 1, 'Rumah Sakit', 'PT. Bangun Mandiri Cemerlang', 'Jl. MT. Haryono, Pontianak', NULL, NULL, NULL, 0, 0, NULL),
(35, 1, 'Gudang', 'PT. WHTB Glass Industry', 'Delta Cikarang', NULL, NULL, NULL, 0, 0, NULL),
(36, 1, 'Cleon Park & Cleon Mansion Site A', 'PT. Mitra Sindo Sukses', 'Jl. Raya Cakung Cilincing Km 0.5', NULL, NULL, NULL, 0, 0, NULL),
(37, 1, 'Hotel', 'PT. Patra Jasa', 'Jl. Sosrowijayan No.35, Yogyakarta', NULL, NULL, NULL, 0, 0, NULL),
(38, 1, 'Jakarta Garden City New East MO, Ruko & Apartment', 'PT. Mitra Sindo Sukses', 'Jl. Raya Cakung Cilincing Km 0.5', NULL, NULL, NULL, 0, 0, NULL),
(39, 1, 'Apartemen Urban Sky', 'PT. Adhi Persada Gedung', 'Cikunir, Bekasi - Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(40, 1, 'Pabrik PT. GCM & PT. EPM', 'PT. Pembangunan Deltamas', 'Kawasan Industri GIIC, Deltamas Blok - Cikarang Pusat', NULL, NULL, NULL, 0, 0, NULL),
(41, 1, 'PMT HPAL Pomala Project', 'Golder Associates', 'Pomalaa, Sulawesi Tenggara', NULL, NULL, NULL, 0, 0, NULL),
(42, 1, 'Benhil Centraya', 'PT. Kurnia Realty Jaya', 'Bendungan Hilir, Jakarta Pusat', NULL, NULL, NULL, 0, 0, NULL),
(43, 1, 'Land Subsidence', 'PT. Geotekindo', 'Semarang, Jawa Tengah', NULL, NULL, NULL, 0, 0, NULL),
(44, 1, 'OCBC NISP Tower', 'Bank OCBC NISP', 'BSD City, Tangerang', NULL, NULL, NULL, 0, 0, NULL),
(45, 1, 'Pembangunan Jalan Tol Harbour Road II Jakarta Utara', 'PT. Wijaya Karya (Persero) Tbk', 'Jakarta Utara', NULL, NULL, NULL, 0, 0, NULL),
(46, 1, 'Tamansari Laswi', 'PT. Wijaya Karya Realty', 'Bandung, Jawa Barat', NULL, NULL, NULL, 0, 0, NULL),
(47, 1, 'Ruko dan Apartemen', 'PT. Rukun Sejahtera ', 'Jl. Raya Seroja No.37, Medan Satria - Bekasi Barat', NULL, NULL, NULL, 0, 0, NULL),
(48, 2, 'Star Shape Dufan', 'PT. Keller Franki Indonesia', 'Ancol, Jakarta Utara', NULL, NULL, NULL, 0, 0, NULL),
(49, 2, 'New Akachan Diapers Factory', 'PT. Pakubumi Semesta', 'Pindodeli', NULL, NULL, NULL, 0, 0, NULL),
(50, 2, 'New Factory Phase II', 'PT. Ervan Prima Abadi', 'KIIC', NULL, NULL, NULL, 0, 0, NULL),
(51, 2, 'Lampung Phase II', 'PT. Paramita Bangun Persada', 'Lampung', NULL, NULL, NULL, 0, 0, NULL),
(52, 2, 'Braling Grand Hotel', 'PT. Astoria Perkasa', 'Purbalingga', NULL, NULL, NULL, 0, 0, NULL),
(53, 2, 'Dermaga Alai Isit', 'PT. Aura Sinar Baru', 'Riau', NULL, NULL, NULL, 0, 0, NULL),
(54, 2, 'Wisma Hunian', 'PT. Kencana Sewu', 'Kebayoran Lama, Jakarta Selatan', NULL, NULL, NULL, 0, 0, NULL),
(55, 2, 'Gedung UIN', 'PT. Pakubumi Semesta', 'Semarang, Jawa Tengah', NULL, NULL, NULL, 0, 0, NULL),
(56, 2, 'RS. Sari Asih', 'PT. Sari Asih', 'Tangerang', NULL, NULL, NULL, 0, 0, NULL),
(57, 2, 'Pabrik', 'PT. Wanjaya Sukses', 'Pindodeli 3, Karawang', NULL, NULL, NULL, 0, 0, NULL),
(58, 2, 'Rusunawa', 'PT. Brantas Abipraya', 'Semarang, Jawa Tengah', NULL, NULL, NULL, 0, 0, NULL),
(59, 2, 'Jembatan Samarangau', 'PT. Kaliraya Sari', 'Kalimantan Timur', NULL, NULL, NULL, 0, 0, NULL),
(60, 2, 'Kampus UPJ', 'PT. Tripondasi Manunggal', 'Bintaro', NULL, NULL, NULL, 0, 0, NULL),
(61, 2, 'Hotel Aston', 'CV. Standart Pile', 'Sorong, Papua', NULL, NULL, NULL, 0, 0, NULL),
(62, 2, 'Melak Bulking Station', 'PT. Paramita Bangun Persada', 'Melak, Kutai Barat', NULL, NULL, NULL, 0, 0, NULL),
(63, 2, 'Sungai Kapuas Mill', 'PT. Cahaya Unggul Prima', 'Jl. Cikini Raya No. 12-14, Menteng', NULL, NULL, NULL, 0, 0, NULL),
(64, 2, 'Kereta Cepat', 'PT. Berdikari Pondasi Perkasa', 'Jakarta - Bandung HSR Section 2', NULL, NULL, NULL, 0, 0, NULL),
(65, 2, 'Pindo Deli 2', 'PT. Pakubumi Semesta', 'Karawang', NULL, NULL, NULL, 0, 0, NULL),
(66, 2, 'Vopak', 'PT. Pakubumi Semesta', 'Tanjung Priok', NULL, NULL, NULL, 0, 0, NULL),
(67, 2, 'Gerbang Tol Cikunir', 'PT. Keller Franki Indonesia', 'Cikunir', NULL, NULL, NULL, 0, 0, NULL),
(68, 2, 'Indah Kiat', 'PT. Pakubumi Semesta', 'Serang', NULL, NULL, NULL, 0, 0, NULL),
(69, 2, 'New BLC', 'PT. Dharma Subur Satya', 'Kaliorang, Kutai Timur', NULL, NULL, NULL, 0, 0, NULL),
(70, 2, 'PLTU 2 - PILe', 'PT. Dharma Subur Satya', 'Cirebon', NULL, NULL, NULL, 0, 0, NULL),
(71, 2, 'Kohler', 'PT. Pakubumi Semesta', 'Deltamas', NULL, NULL, NULL, 0, 0, NULL),
(72, 2, 'Elysee SCBD - CSL', 'PT. Tarumanegara Buanatek', 'SCBD', NULL, NULL, NULL, 0, 0, NULL),
(73, 2, 'Maxone', 'PT. Tarumanegara Buanatek', 'Ambon', NULL, NULL, NULL, 0, 0, NULL),
(74, 2, 'HSR', 'PT. Keller Franki Indonesia', 'Jatiwaringin', NULL, NULL, NULL, 0, 0, NULL),
(75, 2, 'Pemakaian Alat', 'PT. Geo Prima', NULL, NULL, NULL, NULL, 0, 0, NULL),
(76, 2, 'Rumah Tinggal', 'PT. Kencana Sewu', 'Pondok Indah', NULL, NULL, NULL, 0, 0, NULL),
(77, 2, 'Emily 1 (Fuel Terminal & Jetty)', 'PT. Pakubumi Semesta', 'Balikpapan, East Kalimantan', NULL, NULL, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` text,
  `content` text,
  `images` text,
  `lang` varchar(10) NOT NULL DEFAULT 'ID',
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `parent_id`, `name`, `slug`, `description`, `content`, `images`, `lang`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Instrumen Geologi dan Pengujian Lanjutan ubah', 'instrumen-geologi-dan-pengujian-lanjutan-ubah', 'Instrumen Geoteknik dan Pengujian Lapangan banyak digunakan untuk memantau perilaku tanah selama konstruksi. Ini adalah alat penting untuk mengevaluasi dan memverifikasi asumsi desain dan memastikan keselamatan proyek', NULL, 'DSCF4450.jpg', 'ID', 1, '2020-04-20 07:07:03', '2020-07-24 07:34:00'),
(2, 1, 'Geological Instrument and Advanced Testing', 'geological-instrument-and-advanced-testing', 'Geotechnical Instrument and Field Testing are widely used to monitor soil behaviour during construction. This is an important tool to evaluate and verified the design assumption and assure project safety edit', NULL, 'DSCF4450.jpg', 'EN', 1, '2020-04-20 07:07:03', '2020-07-24 07:34:01'),
(3, 3, 'Investigasi Tanah', 'investigasi-tanah', 'Kami menyediakan layanan investigasi tanah yang penting untuk sektor konstruksi untuk informasi tanah dan parameter desain yang terjangkau dan akurat', NULL, 'mesin-bor.jpg', 'ID', 1, '2020-04-20 22:36:07', '2020-07-24 07:36:29'),
(4, 3, 'Soil Investigation', 'soil-investigation', 'We provide essential soil investigation service to construction sector for affordable, precise ground information and design parameters', NULL, 'mesin-bor.jpg', 'EN', 1, '2020-04-20 22:36:07', '2020-07-24 07:36:29'),
(5, 5, 'Pengujian Dinamik Tumpukan Tidak-Merusak', 'pengujian-dinamik-tumpukan-tidak-merusak', 'Disertifikasi oleh Insinyur dari PDI (Pile Dynamic Inc.) AS sejak 1995, kami telah melakukan pengujian dinamis pada pondasi tiang pancang (PDA & PIT) untuk berbagai proyek lepas pantai dan di darat', NULL, 'IMG20200306181431.jpg', 'ID', 1, '2020-04-20 22:40:22', '2020-07-24 07:37:28'),
(6, 5, 'Non-Destructive Test Pile Dynamic Testing', 'non-destructive-test-pile-dynamic-testing', 'Certified by Engineers from PDI (Pile Dynamic Inc.) USA since 1995, we have been conducting dynamic testing on pile foundations (PDA & PIT) for various off-shore and on-shore projects', NULL, 'IMG20200306181431.jpg', 'EN', 1, '2020-04-20 22:40:22', '2020-07-24 07:37:29'),
(7, 7, 'Analisis Teknik Geoteknik', 'analisis-teknik-geoteknik', 'Komitmen untuk memberikan rekayasa nilai klien kami dengan lebih dari 25 tahun pengalaman investigasi tanah, pengeringan, dan pengujian dinamis pada tiang pancang.\r\n\r\nbiaya, keamanan, dan efektivitas adalah alasan utama kami sangat menghargai dengan merekomendasikan pemodelan numerik geoteknik untuk:\r\n\r\n- Deteksi dini masalah geoteknis yang mungkin ditemui selama konstruksi dan waktu hidup.\r\n- Mengoptimalkan desain geoteknik.\r\n- kembali menganalisis perilaku konstruksi geoteknik yang ada.\r\n- penelitian dan penelitian', NULL, 'engineering.png', 'ID', 1, '2020-07-05 01:09:08', '2020-07-24 07:37:50'),
(8, 7, 'Geotchnical Engineering Analysis', 'geotchnical-engineering-analysis', 'A commitment to provide our client value engineering with more than 25 years of experience of soil investigation, dewatering, and dynamic testing on piles.\r\n\r\ncost, safety, and effectiveness are the main reason we highly value by recommending geotechnical numerical modelling to:\r\n\r\n- early detect geotechnical problems that may be encountered during construction and life time.\r\n- optimize the geotechnical design.\r\n- back analyze the behaviour of existing geotechnial construction.\r\n- research and study', NULL, 'engineering.png', 'EN', 1, '2020-07-05 01:09:08', '2020-07-24 07:37:50'),
(9, 9, 'Test Pemompaan & Sistem Dewatering', 'test-pemompaan-sistem-dewatering', 'PT TARUMANEGARA bumiyasa merancang, memasang, dan memelihara sistem pengeringan menggunakan sumur dalam dan atau pompa permukaan untuk hasil penggalian dan konstruksi bawah tanah yang menguntungkan. untuk sistem pengeringan yang tepat, tes pemompaan sangat penting untuk menentukan parameter hidrogeologis. untuk digunakan, pengeringan bukan hanya tentang mengendalikan ketinggian air tanah, ini tentang kesehatan & keamanan lingkungan dan efisiensi biaya', NULL, NULL, 'ID', 1, '2020-07-05 01:49:44', '2020-07-05 01:49:44'),
(10, 9, 'Pumping Test & Dewatering System', 'pumping-test-dewatering-system', 'PT TARUMANEGARA bumiyasa designs, install, and maintaining dewatering system using deep well and or surface pump for favorable outcome of excavation and basement construction. for a proper dewatering system, a pumping test is essential to determine the hydrogeological parameters. to use, dewatering is not just about controlling the ground water level, it is about health & safety environment and cost efficiency', NULL, NULL, 'EN', 1, '2020-07-05 01:49:45', '2020-07-05 01:49:45'),
(11, 11, 'Uji Lab Tanah', 'uji-lab-tanah', 'kualitas tanah di situs Anda memainkan peran penting dalam proyek konstruksi Anda. akibatnya, karakteristik tanah perlu diidentifikasi untuk menentukan kemampuannya untuk mendukung struktur Anda. pengujian tanah dari PT Tarumanegara Bumiyasa memungkinkan Anda menilai kesesuaian tanah, memberikan Anda data penting untuk pengambilan keputusan dan perencanaan yang matang.', NULL, NULL, 'ID', 1, '2020-07-05 01:52:55', '2020-07-05 01:52:55'),
(12, 11, 'Soil Laboratory Test', 'soil-laboratory-test', 'the quality of soil on your site plays a key role in your construction project. as a result, the characteristic of the soil needed to be identified for determine its ability to support your structure. soil testing from PT Tarumanegara Bumiyasa enables you to asses the suitability of the soil, providing you with vital data for informed decision making and planning.', NULL, NULL, 'EN', 1, '2020-07-05 01:52:56', '2020-07-05 01:52:56'),
(13, 13, 'Survei', 'survei', 'PT Tarumanegara Bumiyasa menawarkan layanan survei dan pemetaan yang komprehensif untuk berbagai aplikasi dan tujuan di luar negeri melalui pendekatan terintegrasi untuk pengukuran, analisis dan pengelolaan lokasi dan deskripsi. surveyor tanah, teknisi lapangan, dan personel pemetaan kami dididik dan dilatih tentang keselamatan di tempat kerja', NULL, NULL, 'ID', 1, '2020-07-05 01:56:11', '2020-07-05 01:56:11'),
(14, 13, 'Surveying', 'surveying', 'PT Tarumanegara Bumiyasa offers a comprehensive survey and mapping services for abroad range of applications and purposes through an integrated approach to the measurement, analysis and management of locations and description. our land surveyors, field technicians and mapping personnel are educated and thorougly trained in worksite safety', NULL, NULL, 'EN', 1, '2020-07-05 01:56:11', '2020-07-05 01:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `service_subs`
--

CREATE TABLE `service_subs` (
  `id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `service_id` int DEFAULT NULL,
  `lang` varchar(10) NOT NULL DEFAULT 'ID',
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` text,
  `images` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_subs`
--

INSERT INTO `service_subs` (`id`, `parent_id`, `service_id`, `lang`, `name`, `slug`, `description`, `images`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'ID', 'Uji Pressuremeter', 'uji-pressuremeter', NULL, 'a:1:{i:0;a:5:{s:4:\"name\";s:90:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_uji-pressuremeter_IMG01032-20130424-1600.jpg\";s:12:\"originalName\";s:26:\"IMG01032-20130424-1600.jpg\";s:4:\"size\";i:722136;s:7:\"batchid\";s:36:\"7278276d-4245-420e-b069-cacf8dedc493\";s:4:\"uuid\";s:36:\"07cf490e-e1b0-493b-8f28-3beca24fe80f\";}}', 1, '2020-07-04 19:14:33', '2020-07-04 19:52:23'),
(2, 1, 1, 'EN', 'Pressuremeter Test', 'pressuremeter-test', NULL, 'a:1:{i:0;a:5:{s:4:\"name\";s:90:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_uji-pressuremeter_IMG01032-20130424-1600.jpg\";s:12:\"originalName\";s:26:\"IMG01032-20130424-1600.jpg\";s:4:\"size\";i:722136;s:7:\"batchid\";s:36:\"7278276d-4245-420e-b069-cacf8dedc493\";s:4:\"uuid\";s:36:\"07cf490e-e1b0-493b-8f28-3beca24fe80f\";}}', 1, '2020-07-04 19:14:33', '2020-07-04 19:52:23'),
(3, 3, 1, 'ID', 'Piezomotor', 'piezomotor', NULL, 'a:2:{i:0;a:5:{s:4:\"name\";s:80:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_piezomotor_IMG_20181209_122901.jpg\";s:12:\"originalName\";s:23:\"IMG_20181209_122901.jpg\";s:4:\"size\";i:3315582;s:7:\"batchid\";s:36:\"10abe47a-b883-43fb-932f-4c024949a219\";s:4:\"uuid\";s:36:\"a3e5c6de-0b03-44c5-89a5-bd9c16abe7b4\";}i:1;a:5:{s:4:\"name\";s:80:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_piezomotor_IMG_20181210_174638.jpg\";s:12:\"originalName\";s:23:\"IMG_20181210_174638.jpg\";s:4:\"size\";i:4936770;s:7:\"batchid\";s:36:\"10abe47a-b883-43fb-932f-4c024949a219\";s:4:\"uuid\";s:36:\"65bb1647-8f01-4aa3-bbd2-73f3c4e2b94a\";}}', 1, '2020-07-04 19:59:56', '2020-07-04 20:00:22'),
(4, 3, 1, 'EN', 'Piezomotor', 'piezomotor', NULL, 'a:2:{i:0;a:5:{s:4:\"name\";s:80:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_piezomotor_IMG_20181209_122901.jpg\";s:12:\"originalName\";s:23:\"IMG_20181209_122901.jpg\";s:4:\"size\";i:3315582;s:7:\"batchid\";s:36:\"10abe47a-b883-43fb-932f-4c024949a219\";s:4:\"uuid\";s:36:\"a3e5c6de-0b03-44c5-89a5-bd9c16abe7b4\";}i:1;a:5:{s:4:\"name\";s:80:\"instrumen-geologi-dan-pengujian-lanjutan-ubah_piezomotor_IMG_20181210_174638.jpg\";s:12:\"originalName\";s:23:\"IMG_20181210_174638.jpg\";s:4:\"size\";i:4936770;s:7:\"batchid\";s:36:\"10abe47a-b883-43fb-932f-4c024949a219\";s:4:\"uuid\";s:36:\"65bb1647-8f01-4aa3-bbd2-73f3c4e2b94a\";}}', 1, '2020-07-04 19:59:56', '2020-07-04 20:00:22'),
(5, 5, 3, 'ID', 'CBR', 'cbr', NULL, 'a:3:{i:0;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_141059.jpg\";s:12:\"originalName\";s:19:\"20190211_141059.jpg\";s:4:\"size\";i:2369712;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"68521f44-e62c-48b8-a3f9-48fc8449b6bf\";}i:1;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_143500.jpg\";s:12:\"originalName\";s:19:\"20190211_143500.jpg\";s:4:\"size\";i:3185784;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"7c838404-4775-444f-85e0-ff37d9e1d479\";}i:2;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_143518.jpg\";s:12:\"originalName\";s:19:\"20190211_143518.jpg\";s:4:\"size\";i:2158213;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"a0ffdf3e-e6b5-4023-a345-0450cfa273da\";}}', 1, '2020-07-04 20:01:43', '2020-07-04 20:02:20'),
(6, 5, 3, 'EN', 'CBR', 'cbr', NULL, 'a:3:{i:0;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_141059.jpg\";s:12:\"originalName\";s:19:\"20190211_141059.jpg\";s:4:\"size\";i:2369712;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"68521f44-e62c-48b8-a3f9-48fc8449b6bf\";}i:1;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_143500.jpg\";s:12:\"originalName\";s:19:\"20190211_143500.jpg\";s:4:\"size\";i:3185784;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"7c838404-4775-444f-85e0-ff37d9e1d479\";}i:2;a:5:{s:4:\"name\";s:41:\"investigasi-tanah_cbr_20190211_143518.jpg\";s:12:\"originalName\";s:19:\"20190211_143518.jpg\";s:4:\"size\";i:2158213;s:7:\"batchid\";s:36:\"6eecdec0-1f03-4c90-8cd9-7f7643dc43cc\";s:4:\"uuid\";s:36:\"a0ffdf3e-e6b5-4023-a345-0450cfa273da\";}}', 1, '2020-07-04 20:01:43', '2020-07-04 20:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `telp` text,
  `address` text,
  `fax` text,
  `extra` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `telp`, `address`, `fax`, `extra`) VALUES
(1, 'small-bumiyasa.png', 'tel: (021)15321547<br>\r\ntel: (021)5308689<br>\r\ntel: (021)5308690<br>', 'jl. Raya Kebayoran Lama No.354<br>\r\nSukabumi Utara, Kebon Jeruk<br>\r\nJakarta Barat 11540', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `division_id` int NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `educatons` text,
  `certifications` text,
  `role` varchar(255) DEFAULT NULL,
  `join_year` year DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `division_id`, `image`, `educatons`, `certifications`, `role`, `join_year`, `created_at`, `updated_at`) VALUES
(1, 'Ir. Jo Lian Huat', 1, 'image--011.jpg', NULL, NULL, 'Direktur Utama', NULL, '2020-04-23 01:13:15', '2020-04-23 01:21:44'),
(2, 'Ir. Michel Suhendo P', 1, 'image--012.jpg', NULL, NULL, 'Direktur Operasi', NULL, '2020-04-23 01:22:12', NULL),
(3, 'Ir. Intani Samadhana', 1, 'image--013.jpg', NULL, NULL, 'Wakil Direktur', NULL, '2020-04-23 01:22:35', NULL),
(4, 'Thomas Sunaryo Prasojo, ST', 2, 'image--000.jpg', NULL, NULL, 'General Manager', NULL, '2020-04-23 01:23:02', NULL),
(5, 'Amelia Yuwono, ST, SKOM, MT', 2, 'image--001.jpg', NULL, NULL, 'Engineering Manager', NULL, '2020-04-23 01:23:27', NULL),
(6, 'Jefry Rory Paath, MT', 2, 'image--003.jpg', NULL, NULL, 'Head of Dewatering Division', NULL, '2020-04-23 01:24:06', NULL),
(7, 'Amalia Susanto, ST', 2, 'image--002.jpg', NULL, NULL, 'Head of Soil Investigation Division', NULL, '2020-04-23 01:24:50', NULL),
(8, 'Daniel Tri Purnomo, ST', 2, 'image--004.jpg', NULL, NULL, 'Head of Non Destructive Test Division', NULL, '2020-04-23 01:25:11', NULL),
(9, 'Dede Mahpudin, ST', 3, 'image--008.jpg', NULL, NULL, 'Head of Maintainance and Logistic Division', NULL, '2020-04-23 01:25:39', NULL),
(10, 'Justian Winata, ST', 3, 'image--007.jpg', NULL, NULL, 'Geotechnical Engineer', NULL, '2020-04-23 01:26:03', NULL),
(11, 'Kustiawan, ST', 3, 'image--010.jpg', NULL, NULL, 'Geotechnical Engineer', NULL, '2020-04-23 01:26:28', NULL),
(12, 'Muhammad Ihsan Rezqa, ST', 3, 'image--006.jpg', NULL, NULL, 'Geotechnical Engineer', NULL, '2020-04-23 01:26:51', NULL),
(13, 'Ihsan Riza , ST', 3, 'image--005.jpg', NULL, NULL, 'Geotechnical Engineer', NULL, '2020-04-23 01:27:14', NULL),
(14, 'Adhitya Bayu Siswonegoro, ST', 3, 'image--009.jpg', NULL, NULL, 'Geotechnical Engineer', NULL, '2020-04-23 01:27:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_divisions`
--

CREATE TABLE `team_divisions` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_divisions`
--

INSERT INTO `team_divisions` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Board of Directors', 'board-of-directors', 1, '2020-04-23 00:56:27', '2020-04-23 00:56:27'),
(2, 'Manager & Head of Division', 'manager-head-of-division', 1, '2020-04-23 01:01:25', NULL),
(3, 'Geotechnical Engineer', 'geotechnical-engineer', 1, '2020-04-23 01:01:37', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_categories`
--
ALTER TABLE `client_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_lists`
--
ALTER TABLE `project_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_subs`
--
ALTER TABLE `service_subs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_divisions`
--
ALTER TABLE `team_divisions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `client_categories`
--
ALTER TABLE `client_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_lists`
--
ALTER TABLE `project_lists`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `service_subs`
--
ALTER TABLE `service_subs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `team_divisions`
--
ALTER TABLE `team_divisions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
