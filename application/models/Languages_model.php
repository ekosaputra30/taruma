<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Languages_model extends Base_Model {
  public $table = 'languages';
  public $primary_key = 'id';
  public $fillable = [
    'logo',
    'name',
    'slug',
    'address',
    'fax',
    'extra'
  ];
  public $protected = [];
  public function __construct()
  {
    $this->_database_connection  = 'default';
    $this->return_as = 'object';
    parent::__construct();
  }
}

/* End of file Languages_model.php */
