<div class="app-sidebar sidebar-shadow bg-primary sidebar-text-light">
  <div class="app-header__logo">
    <div class="logo-src"></div>
    <div class="header__pane ml-auto">
      <div>
        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
          <span class="hamburger-box">
              <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>
    </div>
  </div>
  <div class="app-header__mobile-menu">
      <div>
          <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
              <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
              </span>
          </button>
      </div>
  </div>
  <div class="app-header__menu">
    <span>
      <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
        <span class="btn-icon-wrapper">
          <i class="fa fa-ellipsis-v fa-w-6"></i>
        </span>
      </button>
    </span>
  </div>
  <div class="scrollbar-sidebar">
    <div class="app-sidebar__inner">
      <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Companies</li>
        <?php foreach($companies as $company): ?>
          <li class="mm-active">
            <a href="">
              <i class="metismenu-icon lnr-apartment"></i>
              <?= $company->name ?>
              <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul class="mm-show mm-collapse">
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/index') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'index') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  Dashboard
                </a>
              </li>
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/services') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'services') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  Services
                </a>
              </li>
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/projects') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'projects') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  Projects
                </a>
              </li>
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/clients') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'clients') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  Clients
                </a>
              </li>
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/teams') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'teams') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  Team Support
                </a>
              </li>
              <li>
                <a href="<?php echo site_url('backend/'.$company->slug.'/news') ?>" class="<?php echo ($this->uri->segment(2) == $company->slug && $this->uri->segment(3) == 'news') ? 'mm-active' : false ?>">
                  <i class="metismenu-icon"></i>
                  News
                </a>
              </li>
            </ul>
          </li>
        <?php endforeach;?>
      </ul>
    </div>
  </div>
</div>
