<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Language" content="en">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Login - Tarumanegara Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
  />
  <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">
<link href="<?php echo base_url('secure/main.css') ?>" rel="stylesheet"></head>

<body>
  <div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container">
      <div class="h-100">
        <div class="h-100 no-gutters row">
          <div class="h-100 d-flex bg-white justify-content-center align-items-center col">
            <div class="mx-auto app-login-box col-12 col-lg-4 col-xl-2">
              <div class="app-logo"></div>
              <h4 class="mb-0">
                <span class="d-block"><?php echo lang('login_heading');?></span>
                <span><?php echo lang('login_subheading');?></span></h4>
              <h6 class="mt-3">No account? <a href="javascript:void(0);" class="text-primary">Sign up now</a></h6>
              <div class="divider row"></div>
              <div>
                <?php echo form_open("backend/login");?>
                  <div class="form-row">
                    <div class="col-12">
                      <div class="position-relative form-group">
                        <label for="exampleEmail" class=""><?php echo lang('login_identity_label', 'identity');?></label>
                        <?php echo form_input($identity);?>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="position-relative form-group">
                        <label for="examplePassword" class=""><?php echo lang('login_password_label', 'password');?></label>
                        <?php echo form_input($password);?>
                      </div>
                    </div>
                  </div>
                  <div class="position-relative form-check">
                    <?php echo form_checkbox('remember', '1', FALSE, ['id'=>'remember', 'class'=>'form-check-input']);?>
                    <label for="exampleCheck" class="form-check-label"><?php echo lang('login_remember_label', 'remember');?></label>
                  </div>
                  <div class="divider row"></div>
                  <div class="d-flex align-items-center">
                    <div class="mr-auto">
                      <?php echo form_submit('submit', lang('login_submit_btn'), ['class' => 'btn btn-primary  btn-lg']);?>
                      <a href="javascript:void(0);" class="btn-lg btn btn-link"><?php echo lang('login_forgot_password');?></a>
                    </div>
                  </div>
                <?php echo form_close();?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="<?php echo base_url('secure/assets/scripts/main.js') ?>"></script></body>
</html>