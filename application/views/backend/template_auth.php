<?php echo $this->load->view('backend/layout/header', $header, TRUE); ?>
  <div class="app-main">
    <?php echo $this->load->view('backend/sidebar', [], TRUE);?>
    <div class="app-main__outer">
      <div class="app-main__inner bg-heavy-rain">
        <?php echo $this->load->view($bodypage, $datapage, TRUE);?>
      </div>
      <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </div>    
  </div>    
<?php echo $this->load->view('backend/layout/footer', $footer, TRUE); ?>
