<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class Projects extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Projects_model', 'project');
    $this->load->model('backend/Projectlists_model', 'projectlists');
  }

  public function index_get()
  {
    $company = $this->input->get('company');

    $this->_connection = $company;

    $raw_projects = $this->project->on($this->_connection)->get_all();
    $projects = [];

    foreach ($raw_projects as $key => $project) {
      $projects[] = [
        'name' => $project->name,
        'slug' => $project->slug,
        'description' => $project->description,
        'image' => $project->image,
      ];
    }

		$this->response([
			'status' => true,
			'success' => true,
			'projects' => $projects
		], 200);    
  }

  public function detail_get()
  {
    $slug = $this->input->get('slug');
    $company = $this->input->get('company');

    $this->_connection = $company;

    $project = $this->project->on($this->_connection)->where('slug', $slug)->get();
    $projectlists = $this->projectlists->on($this->_connection)->where('project_id', $project->id)->get_all();

		$this->response([
			'status' => true,
			'success' => true,
			'projects' => [
        'row' => $project,
        'lists' => $projectlists
      ]
		], 200);
  }

}

/* End of file Controllername.php */
