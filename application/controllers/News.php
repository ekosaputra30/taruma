<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class News extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/News_model', 'newsmodel');    
  }

  public function index_get()
  {
    $company = $this->input->get('company');

    $this->_connection = $company;
    $lists = $this->newsmodel->on($this->_connection)->group_by('parent_id')->where('lang', 'ID')->get_all();

		$this->response([
			'status' => true,
			'success' => true,
			'news' => $lists
		], 200);
  }

  public function detail_get()
  {
    $slug = $this->input->get('slug');
    $company = $this->input->get('company');

    $this->_connection = $company;
    $news = $this->newsmodel->on($this->_connection)->where(['lang' => 'ID', 'slug' => $slug])->get();

		$this->response([
			'status' => true,
			'success' => true,
			'news' => $news
		], 200);
  }
}