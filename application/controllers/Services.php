<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class Services extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Services_model', 'sm');
    $this->load->model('backend/Servicesubs_model', 'ssubm');
  }

  public function index_get()
  {
    $locale = ($this->input->get('locale')) ? $this->input->get('locale') : 'EN';
    $company = $this->input->get('company');

    $this->_connection = $company;

    $raw_services = $this->sm->on($this->_connection)->where('lang', $locale)->get_all();
    $services = [];

    $img = [];
    foreach ($raw_services as $key => $service) {
      if($company == 'bumiyasa') {
        $sub = $this->ssubm->on($this->_connection)->where(['lang' => $locale, 'service_id' => $service->parent_id])->limit(1);
        if ($sub->count_rows() > 0) {
          $image = unserialize($sub->get()->images);
          $img = base_url('uploads/services/'.$image[0]['name']);
        }
      } else {
        foreach (unserialize($service->images) as $i => $imgs) {
          $img[] = [
            'name' => $imgs['name'],
            'slug' => $service->slug
          ];
        }
        //$img = $image;
      }
      $services[] = [
        'name' => $service->name,
        'slug' => $service->slug,
        'description' => $service->description,
        'images' => base_url('uploads/services/cover/'.$service->images),
      ];
    }

		$this->response([
			'status' => true,
			'success' => true,
      'services' => $services,
      'images' => $img
		], 200);
  }

  public function subdetail_get()
  {
    $locale = ($this->input->get('locale')) ? $this->input->get('locale') : 'EN';
    $slug = $this->input->get('slug');
    $company = $this->input->get('company');

    $this->_connection = $company;

    $service = $this->sm->on($this->_connection)->where(['slug' => $slug])->get();
    $sub = $this->ssubm->on($this->_connection)->where(['lang' => $locale, 'service_id' => $service->parent_id])->get_all();

    $subs = [];
    foreach($sub as $key => $s) {
      $subs[] = [
        'id' => $s->id,
        'service_id' => $s->parent_id,
        'name' => $s->name,
        'slug' => $s->slug,
        'description' => $s->description,
        'images' => unserialize($s->images)
      ];
    }

		$this->response([
			'status' => true,
      'success' => true,
      'subservices' => $subs,
			'service' => $service
		], 200);
  }

  public function detail_get()
  {
    $locale = ($this->input->get('locale')) ? $this->input->get('locale') : 'EN';
    $slug = $this->input->get('slug');
    $company = $this->input->get('company');

    $this->_connection = $company;

    $service = $this->ssubm->on($this->_connection)->where(['lang' => $locale, 'slug' => $slug])->get();
    
    $service_prev = $this->ssubm->on($this->_connection)->where(['lang' => $locale, 'service_id' => $service->parent_id, 'id <' => $service->id])->order_by('id', 'DESC')->limit(1)->get();
    $service_next = $this->ssubm->on($this->_connection)->where(['lang' => $locale, 'service_id' => $service->parent_id, 'id >' => $service->id])->order_by('id')->limit(1)->get();

		$this->response([
			'status' => true,
      'success' => true,
      'services' => [],
      'service' => $service,
      'images' => unserialize($service->images),
      'prev_row' => $service_prev,
      'next_row' => $service_next,
			// 'service' => [
      //   'row' => $service,
      //   'images' => [],
      // ]
		], 200);
  }

}

/* End of file Controllername.php */
