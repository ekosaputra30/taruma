<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class Clients extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Clients_model', 'client');    
    $this->load->model('backend/Clientcategories_model', 'clientcategories');
  }

  public function index_get()
  {
    $company = $this->input->get('company');

    $this->_connection = $company;

    $raw_clients = $this->client->on($this->_connection)->with('clientcategories')->get_all();
    $categories = $this->clientcategories->on($this->_connection)->get_all();
    $lists = [];

    foreach ($raw_clients as $key => $client) {
      $lists[] = [
        'client_name' => $client->name,
        'client_image' => $client->image,
        'client_category' => $client->clientcategories->slug,
        'client_url' => $client->url
      ];
    }

    $categories = $this->_categories($categories);

		$this->response([
			'status' => true,
			'success' => true,
			'categories' => $categories,
			'clients' => $lists
		], 200);
  }

  private function _categories($categories) {
    $lists = [];
    foreach ($categories as $key => $category) {
      $lists[] = [
        'category_name' => $category->name,
        'category_label' => $category->slug,
      ];
    }

    return $lists;
  }

}

/* End of file Controllername.php */
