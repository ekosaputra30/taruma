<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class Teams extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Teams_model', 'team');    
    $this->load->model('backend/Teamdivisions_model', 'division');
  }

  public function index_get()
  {
    $company = $this->input->get('company');

    $this->_connection = $company;
    $raw_divisions = $this->division->on($this->_connection)->with('teams')->get_all();

		$this->response([
			'status' => true,
			'success' => true,
			'division' => $raw_divisions
		], 200);
  }
}