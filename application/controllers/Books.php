<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CONTOH CONTROLLER
| -------------------------------------------------------------------------
|
| Contoh Controller dari dokumentasi REST_Controller (Chris Kacerguis)
|
*/

// Wajib digunakan
require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/core/Base_Rest.php';

class Books extends REST_Controller {
	
	public function __construct()
	{
		parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}
	}
	
	public function index_get()
	{
		$books = [
			[
				'id' => 1,
				'title' => 'book 1',
				'category' => 'history',
			],
			[
				'id' => 2,
				'title' => 'book 2',
				'category' => 'nature',
			]
		];		

		$this->response([
			'status' => true,
			'success' => true,
			'books' => $books
		], 200);
	}

	public function index_post()
	{
		return $this->response([
			'success' => true,
			'message' => 'Data masuk'
		]);
	}
}
