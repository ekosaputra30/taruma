<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/core/Base_Rest.php';
class Setting extends Base_Rest {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Settings_model', 'setting');
  }

  public function index_get()
  {
    $company = $this->input->get('company');

    $this->_connection = $company;
    $raw_settings = $this->setting->on($this->_connection)->as_array()->get(1);

		$this->response([
			'status' => true,
      'success' => true,
			'setting' => $raw_settings
		], 200);
  }
}