<?php

function get_languages() {
  $CI =& get_instance();
  $CI->load->model('Languages_model', 'lm');

  return $CI->lm->get_all();
}

function get_language($lang = 'ID') {
  $CI =& get_instance();
  $CI->load->model('Languages_model', 'lm');

  return $CI->lm->where('code', $lang)->get();
}

/**
 * @return array A CSRF key-value pair
 */
function get_csrf_nonce()
{
  $CI =& get_instance();

  $CI->load->helper('string');
  $key = random_string('alnum', 8);
  $value = random_string('alnum', 20);
  $CI->session->set_flashdata('csrfkey', $key);
  $CI->session->set_flashdata('csrfvalue', $value);

  return [$key => $value];
}

/**
 * @return bool Whether the posted CSRF token matches
 */
function valid_csrf_nonce()
{
  $CI =& get_instance();

  $csrfkey = $CI->input->post($CI->session->flashdata('csrfkey'));
  if ($csrfkey && $csrfkey === $CI->session->flashdata('csrfvalue'))
  {
    return TRUE;
  }
  return FALSE;
}