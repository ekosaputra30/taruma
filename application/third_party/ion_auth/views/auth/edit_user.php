<div class="app-page-title bg-white">
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient pe-7s-user"></div>
      </div>
      <div>
        <?php echo lang('edit_user_heading');?>
        <div class="page-title-subheading">
          <?php echo lang('edit_user_subheading');?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
    <?php echo form_open(uri_string(), ['class'=>'col-12 col-lg-4']);?>
      <legend><h5>User Information</h5></legend>
      <div class="form-group">
        <label for="first_name"><?php echo lang('edit_user_fname_label', 'first_name');?> </label>
        <?php echo form_input($first_name);?>
        <?php echo form_error('first_name');?>
      </div>

      <div class="form-group">
        <label for="last_name"><?php echo lang('edit_user_lname_label', 'last_name');?> </label>
        <?php echo form_input($last_name);?>
        <?php echo form_error('last_name');?>
      </div>

      <div class="form-group">
        <label for="password"><?php echo lang('edit_user_password_label', 'password');?> </label>
        <?php echo form_input($password);?>
        <?php echo form_error('password');?>
      </div>

      <div class="form-group">
        <label for="password_confirm"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
        <?php echo form_input($password_confirm);?>
        <?php echo form_error('password_confirm');?>
      </div>

      <?php if ($this->ion_auth->is_admin()): ?>
        <legend><h5><?php echo lang('edit_user_groups_heading');?></h5></legend>
        <div class="form-group">
          <?php foreach ($groups as $group):?>
            <div class="custom-control custom-control-inline custom-checkbox">
              <?php
                $gID=$group['id'];
                $checked = null;
                $item = null;
                foreach($currentGroups as $grp) {
                  if ($gID == $grp->id) {
                    $checked= ' checked="checked"';
                  break;
                }
              }
              ?>
              <input type="checkbox" id="group-check-<?php echo $group['id'] ?>" class="custom-control-input" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <label class="custom-control-label" for="group-check-<?php echo $group['id'] ?>">
                <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
            </div>
          <?php endforeach?>
        </div>
      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <div class="form-group">
        <?php echo form_submit('submit', lang('edit_user_submit_btn'), ['class' => 'btn btn-primary btn-lg']);?>
      </div>

    <?php echo form_close();?>
  </div>
</div>