<div class="app-page-title bg-white">
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient pe-7s-users"></div>
      </div>
      <div>
        <?php echo lang('create_group_heading');?>
        <div class="page-title-subheading">
          <?php echo lang('create_group_subheading');?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
    <?php echo form_open("backend/auth/create_group", ['class'=>'col-12 col-lg-4']);?>

      <legend><h5>Group Information</h5></legend>
      <div class="form-group">
        <label><?php echo lang('create_group_name_label', 'group_name');?></label>
        <?php echo form_input($group_name);?>
        <?php echo form_error('group_name');?>
      </div>

      <div class="form-group">
        <label><?php echo lang('create_group_desc_label', 'description');?></label>
        <?php echo form_input($description);?>
        <?php echo form_error('description');?>
      </div>

      <div class="form-group">
        <?php echo form_submit('submit', lang('create_group_submit_btn'), ['class' => 'btn btn-primary btn-lg']);?>
      </div>

    <?php echo form_close();?>
  </div>
</div>