<div class="app-page-title bg-white">
	<nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
			<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">Library</a></li>
			<li class="active breadcrumb-item" aria-current="page">Data</li>
		</ol>
	</nav>
	<div class="page-title-wrapper">
		<div class="page-title-heading">
			<div class="page-title-icon">
				<div class="bg-premium-dark icon-gradient pe-7s-users"></div>
			</div>
			<div>
				<?php echo lang('index_heading');?>
				<div class="page-title-subheading">
					<?php echo lang('index_subheading');?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="infoMessage"><?php echo $message;?></div>

<div class="btn-group mb-3">
	<?php echo anchor('backend/auth/create_user', lang('index_create_user_link'), ['class'=>'btn btn-warning btn-square btn-lg'])?>
	&nbsp;
	<?php echo anchor('backend/auth/create_group', lang('index_create_group_link'), ['class'=>'btn btn-warning btn-square btn-lg'])?>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
		<table id="example" class="table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th><?php echo lang('index_groups_th');?></th>
					<th><?php echo lang('index_status_th');?></th>
					<th><?php echo lang('index_action_th');?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user):?>
					<tr>
						<td><?php echo htmlspecialchars($user->first_name.' '.$user->last_name,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("backend/auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'), ['class' => 'badge badge-secondary']) ;?>
							<?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("backend/auth/deactivate/".$user->id, lang('index_active_link'), ['class'=>'border-0 btn-transition btn btn-square btn-sm btn-outline-success']) : anchor("backend/auth/activate/". $user->id, lang('index_inactive_link'), ['class'=>'border-0 btn-transition btn btn-square btn-sm btn-outline-danger']);?></td>
						<td><?php echo anchor("backend/auth/edit_user/".$user->id, 'Edit', ['class' => 'btn btn-success btn-square btn-sm']) ;?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>