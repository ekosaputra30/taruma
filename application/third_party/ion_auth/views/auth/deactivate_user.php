<div class="app-page-title bg-white">
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient pe-7s-user"></div>
      </div>
      <div>
        <?php echo lang('deactivate_heading');?>
        <div class="page-title-subheading">
          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
    <?php echo form_open("backend/auth/deactivate/".$user->id);?>
      <legend>
        <h5><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></h5>
      </legend>

      <div class="form-group">
        <div class="custom-control custom-control-inline custom-radio">
          <input type="radio" class="custom-control-input" name="confirm" id="radio-yes" value="yes" checked="checked" />
          <label class="custom-control-label" for="radio-yes">
            <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
          </label>
        </div>
        <div class="custom-control custom-control-inline custom-radio">
          <input type="radio" class="custom-control-input" name="confirm" id="radio-no" value="no" />
          <label class="custom-control-label" for="radio-no">
            <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
          </label>
        </div>
      </div>

      <?php echo form_hidden($csrf); ?>
      <?php echo form_hidden(['id' => $user->id]); ?>

      <p><?php echo form_submit('submit', lang('deactivate_submit_btn'), ['class' => 'btn btn-primary btn-lg']);?></p>

    <?php echo form_close();?>
  </div>
</div>