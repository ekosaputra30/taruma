<div class="app-page-title bg-white">
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient pe-7s-add-user"></div>
      </div>
      <div>
        <?php echo lang('create_user_heading');?>
        <div class="page-title-subheading">
          <?php echo lang('create_user_subheading');?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
    <?php echo form_open("backend/auth/create_user", ['class'=>'col-12 col-lg-4']);?>

      <legend><h5>User Information</h5></legend>
      <div class="form-group">
        <label for="first_name"><?php echo lang('create_user_fname_label', 'first_name');?></label>
        <?php echo form_input($first_name);?>
        <?php echo form_error('first_name');?>
      </div>

      <div class="form-group">
        <label for="last_name"><?php echo lang('create_user_lname_label', 'last_name');?></label>
        <?php echo form_input($last_name);?>
        <?php echo form_error('last_name');?>
      </div>

      <div class="form-group">
        <label for="email"><?php echo lang('create_user_email_label', 'email');?></label>
        <?php echo form_input($email);?>
        <?php echo form_error('email');?>
      </div>

      <div class="form-group">
        <label for="password"><?php echo lang('create_user_password_label', 'password');?></label>
        <?php echo form_input($password);?>
        <?php echo form_error('password');?>
      </div>

      <div class="form-group">
        <label for="password_confirm"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
        <?php echo form_input($password_confirm);?>
        <?php echo form_error('password_confirm');?>
      </div>

      <div class="form-group">
        <?php echo form_submit('submit', lang('create_user_submit_btn'), ['class' => 'btn btn-primary btn-lg']);?>
      </div>
    <?php echo form_close();?>
  </div>
</div>