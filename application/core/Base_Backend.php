<?php
require APPPATH . '/third_party/MX/Controller.php';

class Base_Backend extends MX_Controller
{
    public $_company;
	public function __construct()
	{
        parent::__construct();
        $this->load->model('backend/Companies_model', 'cm');
        if (!$this->ion_auth->logged_in()) redirect('backend/login', 'refresh');

        $this->_company = ['bumiyasa', 'buanatek'];
    }

    public function get_company($slug) {
        $row = $this->cm->where('slug', $slug)->get();

        return $row;
    }

    public function render($view = false, $data = false) {
        $companies = $this->cm->get_all();
        if ($data === false) {
            $data = [
                'header' => [],
                'footer' => [],
                'datapage' => []
            ];
        }
        $data['header']['companies'] = $companies;

        $dataview = [
            'header' => isset($data['header']) ? $data['header'] : [],
            'bodypage' => $view,
            'datapage' => $data['datapage'],
            'sidebar' => [
                'companies' => $companies
            ],
            'footer' => isset($data['footer']) ? $data['footer'] : []
        ];

        return $this->load->view('backend/template', $dataview, FALSE);
    }

    public function render_auth($view = false, $data = false) {
        if ($data === false) {
            $data = [
                'header' => [],
                'footer' => [],
                'datapage' => []
            ];
        }

        $dataview = [
            'header' => isset($data['header']) ? $data['header'] : [],
            'bodypage' => $view,
            'datapage' => isset($data['datapage']) ? $data['datapage'] : [],
            'footer' => isset($data['footer']) ? $data['footer'] : []
        ];

        return $this->load->view('backend/template_auth', $dataview, FALSE);
    }
}