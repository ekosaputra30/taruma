<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends Base_Model
{
  public $table = 'settings';
  public $primary_key = 'id';
  public $fillable = [
    'logo',
    'telp',
    'address',
    'fax',
    'extra'
  ];
  public $protected = [];
  public $_group = '';
  public function __construct()
  {
    $this->timestamps = FALSE;
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Settings_model.php */
