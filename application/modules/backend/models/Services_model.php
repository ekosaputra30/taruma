<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends Base_Model {
  public $table = 'services';
  public $primary_key = 'id';
  public $fillable = [
    'parent_id',
    'name',
    'slug',
    'description',
    'images',
    'lang',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  { 
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_many['servicesubs'] = [
      'Servicesubs_model',
      'service_id',
      'id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Services_model.php */
