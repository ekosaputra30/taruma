<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teams_model extends Base_Model
{
  public $table = 'teams';
  public $primary_key = 'id';
  public $fillable = [
    'name',
    'division_id',
    'image',
    'educations',
    'certifications',
    'role',
    'join_year',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_one['division'] = [
      'Teamdivisions_model',
      'id',
      'division_id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Teams_model.php */
