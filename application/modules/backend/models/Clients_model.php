<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends Base_Model
{
  public $table = 'clients';
  public $primary_key = 'id';
  public $fillable = [
    'name',
    'slug',
    'url',
    'category_id',
    'image',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_one['clientcategories'] = [
      'Clientcategories_model',
      'id',
      'category_id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Clients_model.php */
