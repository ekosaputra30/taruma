<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projects_model extends Base_Model
{
  public $table = 'projects';
  public $primary_key = 'id';
  public $fillable = [
    'name',
    'slug',
    'description',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_many['projectlists'] = [
      'Projectlists_model',
      'project_id',
      'id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Projects_model.php */
