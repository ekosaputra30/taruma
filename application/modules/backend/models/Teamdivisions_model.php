<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teamdivisions_model extends Base_Model
{
  public $table = 'team_divisions';
  public $primary_key = 'id';
  public $fillable = [
    'name',
    'slug',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_many['teams'] = [
      'Teams_model',
      'division_id',
      'id',
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Teamdivisions_model.php */
