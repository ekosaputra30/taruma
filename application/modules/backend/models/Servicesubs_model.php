<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Servicesubs_model extends Base_Model {
  public $table = 'service_subs';
  public $primary_key = 'id';
  public $fillable = [
    'parent_id',
    'service_id',
    'name',
    'slug',
    'images',
    'lang',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  { 
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_one['services'] = [
      'Services_model',
      'id',
      'service_id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Servicesubs_model.php */
