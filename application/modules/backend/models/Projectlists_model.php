<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projectlists_model extends Base_Model
{
  public $table = 'project_lists';
  public $primary_key = 'id';
  public $fillable = [
    'project_id',
    'name',
    'client',
    'location',
    'start',
    'finish',
    'basement',
    'surface',
    'depth',
    'jasa_per_m',
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    $this->timestamps = FALSE;
    //$this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_one['projects'] = [
      'Projects_model',
      'id',
      'project_id'
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Projectlists_model.php */
