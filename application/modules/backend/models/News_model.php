<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends Base_Model
{
  public $table = 'news';
  public $primary_key = 'id';
  public $fillable = [
    'title',
    'slug',
    'content',
    'image',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    $this->return_as = 'object';
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Clients_model.php */
