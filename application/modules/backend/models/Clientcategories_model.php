<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clientcategories_model extends Base_Model
{
  public $table = 'client_categories';
  public $primary_key = 'id';
  public $fillable = [
    'name',
    'slug',
    'created_at'
  ];
  public $protected = [];
  public $_group = 'bumiyasa';
  public function __construct()
  {
    // $this->_database_connection = $this->_group;
    $this->return_as = 'object';
    $this->has_many['clients'] = [
      'clients_model',
      'category_id',
      'id',
    ];
    parent::__construct();
  }

  public function init($group) {
    return $this->_group = $group;
  }
}

/* End of file Clientcategories_model.php */
