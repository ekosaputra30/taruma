<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Backend {
  
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data['datapage'] = [
      'company' => $this->get_company('bumiyasa')
    ];
    $this->render('bumiyasa/index', $data);
  }

}

/* End of file Index.php */
