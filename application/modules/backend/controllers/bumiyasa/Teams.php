<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Teams_model', 'team');    
    $this->load->model('backend/Teamdivisions_model', 'teamdivision');
    $this->_connection = 'bumiyasa';
  }

  public function index()
  {
    $lists = $this->team->on($this->_connection)->with_division()->get_all();
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('bumiyasa/teams/list', $data);
  }

  public function create() {
    $divisions = $this->teamdivision->on($this->_connection)->get_all();
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('division', 'Division', 'required');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'position' => [
        'id' => 'position',
        'name' => 'position',
        'value' => set_value('position'),
        'class' => (form_error('position')) ? 'form-control is-invalid' : 'form-control'
      ],
      'division' => [
        'name' => 'division',
        'dropdown' => $this->_divisionDropdown($divisions),
        'value' => set_value('division'),
        'extra' => [
          'id' => 'division',
          'class' => (form_error('division')) ? 'form-control is-invalid' : 'form-control'
        ]
      ],
      'image' => [
        'id' => 'image',
        'name' => 'image',
        'value' => set_value('image'),
        'class' => (form_error('image')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'name' => $post['name'],
        'role' => $post['position'],
        'division_id' => $post['division'],
        'created_at' => date('Y-m-d H:i:s')
      ];

      if (!empty($_FILES['image']['name'])) {
        $config['upload_path'] = './uploads/teams/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|webp';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('image')){
          $errors['file'] = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $datainsert['image'] = $upload['file_name'];
        }
      }
      $new_id = $this->team->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'a new client has been added');
			redirect('backend/bumiyasa/teams/edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('bumiyasa')
      ];
      $this->render('bumiyasa/teams/create', $data);
    }
  }

  public function edit($id) {
    $team = $this->team->on($this->_connection)->get($id);
    $divisions = $this->teamdivision->on($this->_connection)->get_all();
    $forms = [];
    $errorfileupload = false;
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('division', 'Division', 'required');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => (set_value('name')) ? set_value('name') : $team->name,
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'position' => [
        'id' => 'position',
        'name' => 'position',
        'value' => (set_value('position')) ? set_value('position') : $team->role,
        'class' => (form_error('position')) ? 'form-control is-invalid' : 'form-control'
      ],
      'division' => [
        'name' => 'division',
        'dropdown' => $this->_divisionDropdown($divisions),
        'value' => (set_value('division')) ? set_value('division') : $team->division_id,
        'extra' => [
          'id' => 'division',
          'class' => (form_error('division')) ? 'form-control is-invalid' : 'form-control'
        ]
      ],
      'image' => [
        'id' => 'image',
        'name' => 'image',
        'value' => (set_value('image')) ? set_value('image') : $team->image,
        'class' => (form_error('image')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $dataupdate = [
        'name' => $post['name'],
        'role' => $post['position'],
        'division_id' => $post['division']
      ];

      if (!empty($_FILES['image']['name'])) {
        $config['upload_path'] = './uploads/teams/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('image')){
          $errorfileupload = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $dataupdate['image'] = $upload['file_name'];
        }
      }
      $this->team->on($this->_connection)->update($dataupdate, $id);

      $this->session->set_flashdata('message', 'team has been updated');
			redirect('backend/bumiyasa/teams/edit/'.$id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'team' => $team,
        'errorfileupload' => $errorfileupload,
        'company' => $this->get_company('bumiyasa')
      ];
      $this->render('bumiyasa/teams/edit', $data);
    }
  }

  public function delete($id) {
    if ($this->team->on($this->_connection)->delete($id)) {
      $this->session->set_flashdata('message', 'team has been deleted');
      redirect("backend/bumiyasa/teams", 'refresh');
    }
  }


  

  public function divisions()
  {
    $lists = $this->teamdivision->on($this->_connection)->with_teams('fields:*count*')->get_all();    
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('bumiyasa/teams/division/list', $data);
  }

  public function division_create() {
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'created_at' => date('Y-m-d H:i:s')
      ];
      $new_id = $this->teamdivision->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'division has been added');
			redirect('backend/bumiyasa/teams/division_edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('bumiyasa')
      ];
      $this->render('bumiyasa/teams/division/create', $data);
    }
  }

  public function division_edit($id) {
    $division = $this->teamdivision->on($this->_connection)->get($id);
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => (set_value('name')) ? set_value('name') : $division->name,
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $dataupdate = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'created_at' => date('Y-m-d H:i:s')
      ];
      $this->teamdivision->on($this->_connection)->update($dataupdate, $id);

      $this->session->set_flashdata('message', 'Division has been updated');
			redirect('backend/bumiyasa/teams/division_edit/'.$id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'division' => $division,
        'company' => $this->get_company('bumiyasa')
      ];
      $this->render('bumiyasa/teams/division/edit', $data);
    }
  }

  private function _divisionDropdown($divisions) {
    $dropdowns = [];
    foreach ($divisions as $key => $division) {
      $dropdowns[$division->id] = $division->name;
    }

    return $dropdowns;
  }
}

/* End of file Teams.php */