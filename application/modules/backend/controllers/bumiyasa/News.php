<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/News_model', 'nm');
    $this->_connection = 'bumiyasa';
  }
  public function index()
  {
    $lists = $this->nm->on($this->_connection)->group_by('parent_id')->where('lang', 'ID')->get_all();
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('bumiyasa/news/list', $data);
  }

  public function create() {
    $forms = [];
    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]');
    $forms = [
      'title' => [
        'id' => 'title',
        'name' => 'title',
        'value' => set_value('title'),
        'class' => (form_error('title')) ? 'form-control is-invalid' : 'form-control'
      ],
      'content' => [
        'id' => 'content',
        'name' => 'content',
        'value' => set_value('content'),
        'class' => (form_error('content')) ? 'form-control is-invalid' : 'form-control'
      ],
      'image' => [
        'id' => 'image',
        'name' => 'image',
        'value' => set_value('image'),
        'class' => (form_error('image')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'title' => $post['title'],
        'slug' => url_title($post['title'], '-', TRUE),
        'content' => $post['content'],
        'created_at' => date('Y-m-d H:i:s')
      ];

      if (!empty($_FILES['image']['name'])) {
        $config['upload_path'] = './uploads/news/';
        $config['file_name'] = time().'_'.url_title($post['title'], '-', TRUE);
        $config['allowed_types'] = 'gif|jpg|jpeg|png|webp';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('image')){
          $errors['file'] = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $datainsert['image'] = $upload['file_name'];
        }
      }
      $new_id = $this->nm->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'a new post has been created');
			redirect('backend/bumiyasa/news/edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('bumiyasa')
      ];
      $data['footer'] = [
        'listjs' => 'backend/bumiyasa/news/listjs'
      ];
      $this->render('bumiyasa/news/create', $data);
    }


  }
}