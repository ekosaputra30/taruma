<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Services_model', 'sm');
    $this->load->model('backend/Servicesubs_model', 'ssubm');
    $this->_connection = 'bumiyasa';
  }

  public function index()
  {
    //$lists = $this->sm->on($this->_connection)->with_servicesubs()->group_by('services.parent_id')->where('lang', 'ID')->get_all();
    $lists = $this->sm->on($this->_connection)->group_by('parent_id')->where('lang', 'ID')->get_all();

    // echo "<pre>";
    // print_r ($lists);
    // echo "</pre>";    

    $services = [];
    foreach($lists as $key => $list) {
      $subservices = $this->ssubm->on($this->_connection)->group_by('parent_id')->where('service_id', $list->parent_id)->get_all();
      $services = $list;
    }
    
    // echo "<pre>";
    // print_r ($services);
    // echo "</pre>";
    // exit;
    
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('bumiyasa/services/list', $data);
  }

  public function create() {

    $forms = [];
    foreach(get_languages() as $lang) {
      $this->form_validation->set_rules('[name]['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      $this->form_validation->set_rules('[description]['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => set_value('name['.$lang->code.']'),
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        'description' => [
          'id' => 'description['.$lang->code.']',
          'name' => 'description['.$lang->code.']',
          'value' => set_value('description['.$lang->code.']'),
          'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ]
      ];
    }
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $parent_id = 0;
      foreach(get_languages() as $lang) {
        $datainsert = [
          'name' => $post['name'][$lang->code],
          'slug' => url_title($post['name'][$lang->code], '-', TRUE),
          'description' => $post['description'][$lang->code],
          'lang' => $lang->code,
          'created_at' => date('Y-m-d H:i:s')
        ];
        $new_id = $this->sm->on($this->_connection)->insert($datainsert);
        if ($parent_id == 0) $parent_id = $new_id;

        $this->sm->on($this->_connection)->where('id', $new_id)->update(['parent_id' => $parent_id]);
      }
			$this->session->set_flashdata('message', 'service has been added');
			redirect('backend/bumiyasa/services/edit/'.$parent_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('bumiyasa')
      ];
      $this->render('bumiyasa/services/create', $data);
    }
  }

  public function create_sub($id) {
    $service = $this->sm->on($this->_connection)->get($id);
    $forms = [];
    foreach(get_languages() as $lang) {
      $this->form_validation->set_rules('[name]['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      //$this->form_validation->set_rules('[description]['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => set_value('name['.$lang->code.']'),
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        // 'description' => [
        //   'id' => 'description['.$lang->code.']',
        //   'name' => 'description['.$lang->code.']',
        //   'value' => set_value('description['.$lang->code.']'),
        //   'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        // ]
      ];
    }

    if ($this->form_validation->run() === TRUE) {
      $post = $this->input->post();
      $parent_id = 0;
      foreach(get_languages() as $lang) {
        $datainsert = [
          'service_id' => $id,
          'name' => $post['name'][$lang->code],
          'slug' => url_title($post['name'][$lang->code], '-', TRUE),
          'lang' => $lang->code,
          'created_at' => date('Y-m-d H:i:s')
        ];
        $new_id = $this->ssubm->on($this->_connection)->insert($datainsert);
        if ($parent_id == 0) $parent_id = $new_id;

        $this->ssubm->on($this->_connection)->where('id', $new_id)->update(['parent_id' => $parent_id]);
      }
			$this->session->set_flashdata('message', 'sub service has been added');
			redirect('backend/bumiyasa/services/edit_sub/'.$parent_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'service' => $service,
        'company' => $this->get_company('bumiyasa')
      ];
      $data['header'] = [
        'listcss' => 'backend/bumiyasa/services/listcss'
      ];
      $data['footer'] = [
        'listjs' => 'backend/bumiyasa/services/listjs'
      ];
      $this->render('bumiyasa/services/create_sub', $data);
    }
  }

  public function edit($id) {
    $service = $this->sm->on($this->_connection)->with_servicesubs('where:lang=\'ID\'')->get($id);
    $services = $this->sm->on($this->_connection)->where('parent_id', $id)->get_all();
    //$servicesubs = $this->ssubm->on($this->_connection)->group_by('parent_id')->where('lang', 'ID')->order_by('service_id')->get_all();
    $errors = [];
    $forms = [];
    foreach(get_languages() as $key => $lang) {
      $this->form_validation->set_rules('name['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      $this->form_validation->set_rules('description['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => ($services->$key->lang == $lang->code) ? $services->$key->name : false,
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        'description' => [
          'id' => 'description['.$lang->code.']',
          'name' => 'description['.$lang->code.']',
          'value' => ($services->$key->lang == $lang->code) ? $services->$key->description : false,
          'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
      ];
    }

    if (isset($_POST) && !empty($_POST))
    {
      $post = $this->input->post();      
      if ($id != $this->input->post('parent_id'))
			{
				show_error('This form post did not pass our security checks');
      }
      
      if ($this->form_validation->run() == TRUE) {
        $dataupdate_images = "";
        if (!empty($_FILES['image']['name'])) {
          
          $config['upload_path'] = './uploads/services/cover/';
          $config['allowed_types'] = 'gif|jpg|jpeg|png';
          $config['remove_spaces'] = false;
          $this->load->library('upload', $config);
        
          if ( ! $this->upload->do_upload('image')){
            $errors = $this->upload->display_errors();
            
            echo "<pre>";
            print_r ($errors);
            echo "</pre>";
            exit;
            
          }
          else{
            $upload = $this->upload->data();
            $dataupdate_images = $upload['file_name'];
          }
        }   
        foreach(get_languages() as $lang) {
          $dataupdate = [
            'name' => $post['name'][$lang->code],
            'slug' => url_title($post['name'][$lang->code], '-', TRUE),
            'description' => $post['description'][$lang->code],
            'images' => $dataupdate_images,
            'lang' => $lang->code,
          ];
          $this->sm->on($this->_connection)->where('id', $post['id'][$lang->code])->update($dataupdate);
        }
        $this->session->set_flashdata('message', 'service has been updated');
        //redirect("backend/bumiyasa/services/edit/".$id, 'refresh');
      }
    }
      
    $data['datapage'] = [
      'id' => $id,
      'form' => $forms,
      'errors' => $errors,
      'service' => $service,
      'services' => $services,
      'company' => $this->get_company('bumiyasa')
    ];
    $this->render('bumiyasa/services/edit', $data);
  }

  public function edit_sub($id) {
    $servicesub = $this->ssubm->on($this->_connection)->get($id);
    $servicesubs = $this->ssubm->on($this->_connection)->where('parent_id', $id)->get_all();
    $service = $this->sm->on($this->_connection)->where('parent_id', $servicesub->service_id)->get();
    
    $forms = [];
    foreach(get_languages() as $key => $lang) {
      $this->form_validation->set_rules('name['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      // $this->form_validation->set_rules('description['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => ($servicesubs->$key->lang == $lang->code) ? $servicesubs->$key->name : false,
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        // 'description' => [
        //   'id' => 'description['.$lang->code.']',
        //   'name' => 'description['.$lang->code.']',
        //   'value' => ($servicesubs->$key->lang == $lang->code) ? $servicesubs->$key->description : false,
        //   'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        // ]
      ];
    }

    if (isset($_POST) && !empty($_POST))
    {
      $post = $this->input->post();      
      if ($id != $this->input->post('parent_id'))
			{
				show_error('This form post did not pass our security checks');
      }
      
      if ($this->form_validation->run() == TRUE) {
        foreach(get_languages() as $lang) {
          $dataupdate = [
            'name' => $post['name'][$lang->code],
            'slug' => url_title($post['name'][$lang->code], '-', TRUE),
            //'description' => $post['description'][$lang->code],
            'lang' => $lang->code,
          ];
          $this->ssubm->on($this->_connection)->where('id', $post['id'][$lang->code])->update($dataupdate);
        }
        $this->session->set_flashdata('message', 'sub service has been updated');
        redirect("backend/bumiyasa/services/edit_sub/".$post['parent_id'], 'refresh');
      }
    }

    $data['datapage'] = [
      'id' => $id,
      'form' => $forms,
      'service' => $service,
      'servicesub' => $servicesub,
      'servicesubs' => $servicesubs,
      'galleries' => ($servicesub->images != NULL) ? unserialize($servicesub->images) : array(),
      'company' => $this->get_company('bumiyasa')
    ];
    $data['header'] = [
      'listcss' => 'backend/bumiyasa/services/listcss'
    ];
    $data['footer'] = [
      'listjs' => 'backend/bumiyasa/services/listjs'
    ];
    $this->render('bumiyasa/services/edit_sub', $data);

  }

  public function add_images($id) {
    $servicesub = $this->ssubm->on($this->_connection)->get($id);
    $service = $this->sm->on($this->_connection)->where('parent_id', $servicesub->service_id)->get();

    $config['upload_path'] = './uploads/services/';
    $config['file_name'] = $service->slug.'_'.$servicesub->slug.'_'.$this->input->post('qqfilename');
    $config['allowed_types'] = 'gif|jpg|png';
    $config['remove_spaces'] = false;
    
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload('qqfile')){
      $error = array('error' => $this->upload->display_errors());
      echo json_encode(['error'=>$error]);
    }
    else{
      $configimg = [];
      $configimg['image_library'] = 'gd2';
      $configimg['source_image'] = './uploads/services/'.$config['file_name'];
      $configimg['create_thumb'] = FALSE;
      $configimg['quality'] = '50%';
      $configimg['maintain_ratio'] = TRUE;
      $configimg['width']         = 1280;

      $this->load->library('image_lib', $configimg);

      if ( !$this->image_lib->resize() ) {
        echo json_encode(['error'=>$this->image_lib->display_errors()]);
      } else {
        echo json_encode(['success'=>true]);
      }
    }
  }

  public function save_galleries($id) {
    $servicesub = $this->ssubm->on($this->_connection)->get($id);
    $service = $this->sm->on($this->_connection)->where('parent_id', $servicesub->service_id)->get();
    
    $images = ($servicesub->images != NULL) ? unserialize($servicesub->images) : [];
    $posts = json_decode(file_get_contents('php://input'));
    $datainsert = [];
    foreach($posts as $post) {
      $datainsert = [
        'name' => $service->slug.'_'.$servicesub->slug.'_'.$post->name,
        'originalName' => $post->originalName,
        'size' => $post->size,
        'batchid' => $post->batchId,
        'uuid' => $post->uuid
      ];
      array_push($images, $datainsert);
    }
    $jsonable = serialize($images);
    $this->ssubm->on($this->_connection)->where('parent_id', $id)->update(['images'=>$jsonable]);
  }

  public function delete($id) {
    if ($this->sm->on($this->_connection)->delete($id)) {
      $this->session->set_flashdata('message', 'service has been deleted');
      redirect("backend/bumiyasa/services", 'refresh');
    }
  }

  public function remove_image($id, $uuid) {
    $servicesub = $this->ssubm->on($this->_connection)->get($id);
    $images = ($servicesub->images != NULL) ? unserialize($servicesub->images) : [];
    $newimages = [];

    $key = array_search($uuid, array_column($images, 'uuid'));
    unlink('uploads/services/'.$images[$key]['name']);
    unset($images[$key]);

    foreach($images as $image) {
      $datainsert = [
        'name' => $image['name'],
        'originalName' => $image['originalName'],
        'size' => $image['size'],
        'batchid' => $image['batchId'],
        'uuid' => $image['uuid']
      ];
      array_push($newimages, $datainsert);
    }

    $jsonable = serialize($newimages);
    if ($this->ssubm->on($this->_connection)->where('parent_id', $id)->update(['images'=>$jsonable])) {
      $this->session->set_flashdata('message', 'an Image has been deleted');
      redirect('backend/bumiyasa/services/edit_sub/'.$id, 'refresh');
    }
  }

  public function delete_galleries($id) {
    $servicesub = $this->ssubm->on($this->_connection)->get($id);
    $images = ($servicesub->images != NULL) ? unserialize($servicesub->images) : [];

    foreach($images as $image) {
      unlink('uploads/services/'.$image['name']);
    }

    if ($this->ssubm->on($this->_connection)->where('parent_id', $id)->update(['images'=>null])) {
      $this->session->set_flashdata('message', 'Image(s) has been deleted');
      redirect('backend/bumiyasa/services/edit_sub/'.$id, 'refresh');
    }
  }

}

/* End of file Services.php */
