<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Settings_model', 'sm');    
    $this->_connection = 'buanatek'; 
  }

  public function index()
  {
    $forms = [];
    $errors = [];
    $setting = $this->sm->on($this->_connection)->get_all();
    $this->form_validation->set_rules('telp', 'telp', 'trim|required');
    $this->form_validation->set_rules('address', 'address', 'trim|required');
    
    if (empty($_FILES['logo']['name'])) {
      $this->form_validation->set_rules('logo', 'Logo', 'required');
    }
    $i = 0;

    $forms = [
      'logo' => [
        'id' => 'logo',
        'name' => 'logo',
        'value' => ($this->sm->on($this->_connection)->count_rows() > 0) ? $setting->$i->logo : set_value('logo'),
        'class' => (form_error('logo')) ? 'form-control is-invalid' : 'form-control'
      ],
      'telp' => [
        'id' => 'telp',
        'name' => 'telp',
        'value' => ($this->sm->on($this->_connection)->count_rows() > 0) ? $setting->$i->telp : set_value('telp'),
        'class' => (form_error('telp')) ? 'form-control is-invalid' : 'form-control'
      ],
      'address' => [
        'id' => 'address',
        'name' => 'address',
        'value' => ($this->sm->on($this->_connection)->count_rows() > 0) ? $setting->$i->address : set_value('address'),
        'class' => (form_error('address')) ? 'form-control is-invalid' : 'form-control'
      ],
    ];

    if ($this->form_validation->run() === TRUE) {
      $post = $this->input->post();

      $datasetting = [
        'telp' => $post['telp'],
        'address' => $post['address'],
      ];

      if (!empty($_FILES['logo']['name'])) {
        $config['upload_path'] = './uploads/settings/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('logo')){
          $errors['file'] = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $datasetting['logo'] = $upload['file_name'];
        }
      }

      if ($this->sm->on($this->_connection)->count_rows() > 0) {
        $this->sm->on($this->_connection)->update($datasetting, 1);
      } else {
        $this->sm->on($this->_connection)->insert($datasetting);
      }

			$this->session->set_flashdata('message', 'service has been added');
			redirect('backend/buanatek/setting', 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'logo' => ($this->sm->on($this->_connection)->count_rows() > 0) ? $setting->$i->logo : false,
        'company' => $this->get_company('buanatek')
      ];

      $this->render('buanatek/setting/form', $data);
    }
  }

}

/* End of file Setting.php */
