<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Services_model', 'sm');    
    $this->_connection = 'buanatek';
  }

  public function index()
  {    
    $data['datapage'] = [
      'lists' => $this->sm->on($this->_connection)->group_by('parent_id')->where('lang', 'ID')->get_all()
    ];
    $this->render('buanatek/services/list', $data);
  }

  public function create() {

    $forms = [];
    foreach(get_languages() as $lang) {
      $this->form_validation->set_rules('[name]['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      $this->form_validation->set_rules('[description]['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => set_value('name['.$lang->code.']'),
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        'description' => [
          'id' => 'description['.$lang->code.']',
          'name' => 'description['.$lang->code.']',
          'value' => set_value('description['.$lang->code.']'),
          'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ]
      ];
    }
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $parent_id = 0;
      foreach(get_languages() as $lang) {
        $datainsert = [
          'name' => $post['name'][$lang->code],
          'slug' => url_title($post['name'][$lang->code], '-', TRUE),
          'description' => $post['description'][$lang->code],
          'lang' => $lang->code,
          'created_at' => date('Y-m-d H:i:s')
        ];
        $new_id = $this->sm->on($this->_connection)->insert($datainsert);
        if ($parent_id == 0) $parent_id = $new_id;

        $this->sm->on($this->_connection)->where('id', $new_id)->update(['parent_id' => $parent_id]);
      }
			$this->session->set_flashdata('message', 'service has been added');
			redirect('backend/buanatek/services/edit/'.$parent_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/services/create', $data);
    }
  }

  public function edit($id) {
    $service = $this->sm->on($this->_connection)->get($id);
    $services = $this->sm->on($this->_connection)->where('parent_id', $id)->get_all();
    $forms = [];
    foreach(get_languages() as $key => $lang) {
      $this->form_validation->set_rules('name['.$lang->code.']', 'Name '.$lang->code, 'trim|required|min_length[3]');
      $this->form_validation->set_rules('description['.$lang->code.']', 'Description '.$lang->code, 'trim|required');

      $forms[$lang->code] = [
        'name' => [
          'id' => 'name['.$lang->code.']',
          'name' => 'name['.$lang->code.']',
          'value' => ($services->$key->lang == $lang->code) ? $services->$key->name : false,
          'class' => (form_error('name['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ],
        'description' => [
          'id' => 'description['.$lang->code.']',
          'name' => 'description['.$lang->code.']',
          'value' => ($services->$key->lang == $lang->code) ? $services->$key->description : false,
          'class' => (form_error('description['.$lang->code.']')) ? 'form-control is-invalid' : 'form-control'
        ]
      ];
    }

    if (isset($_POST) && !empty($_POST))
    {
      $post = $this->input->post();      
      if ($id != $this->input->post('parent_id'))
			{
				show_error('This form post did not pass our security checks');
      }
      
      if ($this->form_validation->run() == TRUE) {      
        foreach(get_languages() as $lang) {
          $dataupdate = [
            'name' => $post['name'][$lang->code],
            'slug' => url_title($post['name'][$lang->code], '-', TRUE),
            'description' => $post['description'][$lang->code],
            'lang' => $lang->code,
          ];
          $this->sm->on($this->_connection)->where('id', $post['id'][$lang->code])->update($dataupdate);
        }
        $this->session->set_flashdata('message', 'service has been updated');
        redirect("backend/buanatek/services/edit/".$id, 'refresh');
      }
    }
      
    $data['datapage'] = [
      'id' => $id,
      'form' => $forms,
      'service' => $service,
      'services' => $services,
      'galleries' => ($service->images != NULL) ? unserialize($service->images) : array(),
      'company' => $this->get_company('buanatek')
    ];
    $data['header'] = [
      'listcss' => 'backend/buanatek/services/listcss'
    ];
    $data['footer'] = [
      'listjs' => 'backend/buanatek/services/listjs'
    ];
    $this->render('buanatek/services/edit', $data);
  }

  public function add_images() {
    
    $config['upload_path'] = './uploads/services/';
    $config['file_name'] = $this->input->post('qqfilename');
    $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
    $config['remove_spaces'] = false;
    
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload('qqfile')){
      $error = array('error' => $this->upload->display_errors());
      echo json_encode(['error'=>$error]);
    }
    else{
      $configimg = [];
      $configimg['image_library'] = 'gd2';
      $configimg['source_image'] = './uploads/services/'.$config['file_name'];
      $configimg['create_thumb'] = FALSE;
      $configimg['quality'] = '50%';
      $configimg['maintain_ratio'] = TRUE;
      $configimg['width']         = 1280;

      $this->load->library('image_lib', $configimg);

      if ( !$this->image_lib->resize() ) {
        echo json_encode(['error'=>$this->image_lib->display_errors()]);
      } else {
        echo json_encode(['success'=>true]);
      }
    }
  }

  public function save_galleries($id) {
    $service = $this->sm->on($this->_connection)->get($id);
    $images = ($service->images != NULL) ? unserialize($service->images) : [];
    $posts = json_decode(file_get_contents('php://input'));
    $datainsert = [];
    foreach($posts as $post) {
      $datainsert = [
        'name' => $post->name,
        'originalName' => $post->originalName,
        'size' => $post->size,
        'batchid' => $post->batchId,
        'uuid' => $post->uuid
      ];
      array_push($images, $datainsert);
    }
    $jsonable = serialize($images);
    $this->sm->on($this->_connection)->where('parent_id', $id)->update(['images'=>$jsonable]);
  }

  public function delete($id) {
    if ($this->sm->on($this->_connection)->delete($id)) {
      $this->session->set_flashdata('message', 'service has been deleted');
      redirect("backend/buanatek/services", 'refresh');
    }
  }

  public function remove_image($id, $uuid) {
    $service = $this->sm->on($this->_connection)->get($id);
    $images = ($service->images != NULL) ? unserialize($service->images) : [];
    $newimages = [];

    $key = array_search($uuid, array_column($images, 'uuid'));
    unlink('uploads/services/'.$images[$key]['name']);
    unset($images[$key]);

    foreach($images as $image) {
      $datainsert = [
        'name' => $image['name'],
        'originalName' => $image['originalName'],
        'size' => $image['size'],
        'batchid' => $image['batchId'],
        'uuid' => $image['uuid']
      ];
      array_push($newimages, $datainsert);
    }

    $jsonable = serialize($newimages);
    if ($this->sm->on($this->_connection)->where('parent_id', $id)->update(['images'=>$jsonable])) {
      $this->session->set_flashdata('message', 'an Image has been deleted');
      redirect('backend/buanatek/services/edit/'.$id, 'refresh');
    }
  }

  public function delete_galleries($id) {
    $service = $this->sm->on($this->_connection)->get($id);
    $images = ($service->images != NULL) ? unserialize($service->images) : [];

    foreach($images as $image) {
      unlink('uploads/services/'.$image['name']);
    }

    if ($this->sm->on($this->_connection)->where('parent_id', $id)->update(['images'=>null])) {
      $this->session->set_flashdata('message', 'Image(s) has been deleted');
      redirect('backend/buanatek/services/edit/'.$id, 'refresh');
    }
  }

}

/* End of file Services.php */
