<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends Base_Backend {
  private $_connection;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Projects_model', 'project');    
    $this->load->model('backend/Projectlists_model', 'projectlists');    
    $this->_connection = 'buanatek';
  }

  public function index()
  {
    $lists = $this->project->on($this->_connection)->with_projectlists('fields:*count*')->get_all();
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('buanatek/projects/list', $data);
  }

  public function lists($id)
  {
    $list = $this->project->on($this->_connection)->get($id);
    $lists = $this->projectlists->on($this->_connection)->where('project_id', $id)->with_projects()->get_all();    
    $data['datapage'] = [
      'id' => $id,
      'list' => $list,
      'lists' => $lists
    ];
    $this->render('buanatek/projects/lists', $data);
  }

  public function create() {
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'description' => [
        'id' => 'description',
        'name' => 'description',
        'value' => set_value('description'),
        'class' => (form_error('description')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'description' => $post['description'],
        'created_at' => date('Y-m-d H:i:s')
      ];
      $new_id = $this->project->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'project has been added');
			redirect('backend/buanatek/projects/edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/projects/create', $data);
    }
  }

  public function create_list($id) {
    $project = $this->project->on($this->_connection)->get($id);
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('client', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('location', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'client' => [
        'id' => 'client',
        'name' => 'client',
        'value' => set_value('client'),
        'class' => (form_error('client')) ? 'form-control is-invalid' : 'form-control'
      ],
      'location' => [
        'id' => 'location',
        'name' => 'location',
        'value' => set_value('location'),
        'class' => (form_error('location')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {
      $post = $this->input->post();
      $datainsert = [
        'project_id' => $post['project_id'],
        'name' => $post['name'],
        'client' => $post['name'],
        'location' => $post['location'],
      ];
      $new_id = $this->projectlists->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'project has been added');
			redirect('backend/buanatek/projects/lists/'.$id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'project' => $project,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/projects/create_list', $data);
    }
  }

  public function edit($id) {
    $project = $this->project->on($this->_connection)->get($id);
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => (set_value('name')) ? set_value('name') : $project->name,
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'description' => [
        'id' => 'description',
        'name' => 'description',
        'value' => (set_value('description')) ? set_value('description') : $project->description,
        'class' => (form_error('description')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];

    if (isset($_POST) && !empty($_POST))
    {
      $post = $this->input->post();      
      if ($id != $this->input->post('id'))
			{
				show_error('This form post did not pass our security checks');
      }
      
      if ($this->form_validation->run() == TRUE) {
        $dataupdate = [
          'name' => $post['name'],
          'slug' => url_title($post['name'], '-', TRUE),
          'description' => $post['description'],
        ];
        $this->project->on($this->_connection)->where('id', $post['id'])->update($dataupdate);
        $this->session->set_flashdata('message', 'project has been updated');
        redirect("backend/buanatek/projects/edit/".$id, 'refresh');
      }
    }
    $data['datapage'] = [
      'form' => $forms,
      'project' => $project,
      'company' => $this->get_company('buanatek')
    ];
    $this->render('buanatek/projects/edit', $data);
  }

  public function delete($id) {
    if ($this->project->on($this->_connection)->delete($id)) {
      $this->session->set_flashdata('message', 'service has been deleted');
      redirect("backend/buanatek/projects", 'refresh');
    }
  }
}

/* End of file Projects.php */
