<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends Base_Backend {
  private $_connection;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backend/Clients_model', 'client');    
    $this->load->model('backend/Clientcategories_model', 'clientcategories');
    $this->_connection = 'buanatek';
  }

  public function index()
  {
    $lists = $this->client->on($this->_connection)->with_clientcategories()->get_all();
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('buanatek/clients/list', $data);
  }

  public function create() {
    $categories = $this->clientcategories->on($this->_connection)->get_all();
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('category', 'Category', 'required');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'url' => [
        'id' => 'url',
        'name' => 'url',
        'value' => set_value('url'),
        'class' => (form_error('url')) ? 'form-control is-invalid' : 'form-control'
      ],
      'category' => [
        'name' => 'category',
        'dropdown' => $this->_categoryDropdown($categories),
        'value' => set_value('category'),
        'extra' => [
          'id' => 'category',
          'class' => (form_error('category')) ? 'form-control is-invalid' : 'form-control'
        ]
      ],
      'image' => [
        'id' => 'image',
        'name' => 'image',
        'value' => set_value('image'),
        'class' => (form_error('image')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'url' => $post['url'],
        'category_id' => $post['category'],
        'created_at' => date('Y-m-d H:i:s')
      ];

      if (!empty($_FILES['image']['name'])) {
        $config['upload_path'] = './uploads/clients/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|webp';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('image')){
          $errors['file'] = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $datainsert['image'] = $upload['file_name'];
        }
      }
      $new_id = $this->client->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'a new client has been added');
			redirect('backend/buanatek/clients/edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/clients/create', $data);
    }
  }

  public function edit($id) {
    $client = $this->client->on($this->_connection)->get($id);
    $categories = $this->clientcategories->on($this->_connection)->get_all();
    $forms = [];
    $errorfileupload = false;
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('category', 'Category', 'required');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => (set_value('name')) ? set_value('name') : $client->name,
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ],
      'url' => [
        'id' => 'url',
        'name' => 'url',
        'value' => (set_value('url')) ? set_value('url') : $client->url,
        'class' => (form_error('url')) ? 'form-control is-invalid' : 'form-control'
      ],
      'category' => [
        'name' => 'category',
        'dropdown' => $this->_categoryDropdown($categories),
        'value' => (set_value('category')) ? set_value('category') : $client->category_id,
        'extra' => [
          'id' => 'category',
          'class' => (form_error('category')) ? 'form-control is-invalid' : 'form-control'
        ]
      ],
      'image' => [
        'id' => 'image',
        'name' => 'image',
        'value' => (set_value('image')) ? set_value('image') : $client->image,
        'class' => (form_error('image')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $dataupdate = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'url' => $post['url'],
        'category_id' => $post['category']
      ];

      if (!empty($_FILES['image']['name'])) {
        $config['upload_path'] = './uploads/clients/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['remove_spaces'] = false;
        $this->load->library('upload', $config);
      
        if ( ! $this->upload->do_upload('image')){
          $errorfileupload = $this->upload->display_errors();
        }
        else{
          $upload = $this->upload->data();
          $dataupdate['image'] = $upload['file_name'];
        }
      }
      $this->client->on($this->_connection)->update($dataupdate, $id);

      $this->session->set_flashdata('message', 'a new client has been updated');
			redirect('backend/buanatek/clients/edit/'.$id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'client' => $client,
        'errorfileupload' => $errorfileupload,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/clients/edit', $data);
    }
  }

  public function delete($id) {
    if ($this->client->on($this->_connection)->delete($id)) {
      $this->session->set_flashdata('message', 'client has been deleted');
      redirect("backend/buanatek/clients", 'refresh');
    }
  }
  


  public function categories()
  {
    $lists = $this->clientcategories->on($this->_connection)->with_clients()->get_all();
    $data['datapage'] = [
      'lists' => $lists
    ];
    $this->render('buanatek/clients/category/list', $data);
  }

  public function category_create() {
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => set_value('name'),
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $datainsert = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'created_at' => date('Y-m-d H:i:s')
      ];
      $new_id = $this->clientcategories->on($this->_connection)->insert($datainsert);

      $this->session->set_flashdata('message', 'category has been added');
			redirect('backend/buanatek/clients/category_edit/'.$new_id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/clients/category/create', $data);
    }
  }

  public function category_edit($id) {
    $category = $this->clientcategories->on($this->_connection)->get($id);
    $forms = [];
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $forms = [
      'name' => [
        'id' => 'name',
        'name' => 'name',
        'value' => (set_value('name')) ? set_value('name') : $category->name,
        'class' => (form_error('name')) ? 'form-control is-invalid' : 'form-control'
      ]
    ];
    
    if ($this->form_validation->run() === TRUE) {  
      $post = $this->input->post();
      $dataupdate = [
        'name' => $post['name'],
        'slug' => url_title($post['name'], '-', TRUE),
        'created_at' => date('Y-m-d H:i:s')
      ];
      $this->clientcategories->on($this->_connection)->update($dataupdate, $id);

      $this->session->set_flashdata('message', 'category has been updated');
			redirect('backend/buanatek/clients/category_edit/'.$id, 'refresh');
    } else {
      $data['datapage'] = [
        'form' => $forms,
        'category' => $category,
        'company' => $this->get_company('buanatek')
      ];
      $this->render('buanatek/clients/category/edit', $data);
    }
  }

  private function _categoryDropdown($categories) {
    $dropdowns = [];
    foreach ($categories as $key => $category) {
      $dropdowns[$category->id] = $category->name;
    }

    return $dropdowns;
  }
}

/* End of file Clients.php */