<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends Base_Backend {
  
    public function __construct()
    {
      parent::__construct();
    }

    public function index()
    {
        $this->render('index');
    }

}

/* End of file Backend.php */
