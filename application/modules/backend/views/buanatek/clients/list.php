<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/index') ?>">Buanatek</a></li>
			<li class="active breadcrumb-item" aria-current="page">Clients</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Buanatek &mdash; Clients
        <div class="page-title-subheading">
          Client Lists
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
  <div class="card-header">
    <?= anchor('backend/buanatek/clients/create', 'add client', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
    &nbsp;
    <?= anchor('backend/buanatek/clients/category_create', 'add category', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
  </div>

  <div class="card-body">
    <table id="example" class="table table-hover table-striped table-borderes">
      <thead>
        <tr>
          <th>#</th>
          <th>Client Name</th>
          <th>Category</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if($lists): ?>
          <?php foreach($lists as $key => $client): ?>
            <?php $i = 0; ?>
            <tr>
              <td><?= $key+1  ?></td>
              <td><?= $client->name ?></td>
              <td><?= $client->clientcategories->name ?></td>
              <td>
                <?= anchor('backend/buanatek/clients/edit/'.$client->id, 'Detail', ['class'=>'btn btn-square btn-success btn-sm']); ?>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>
          <tr class="text-center">
            <td colspan="3">no data</td>
          </tr>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>