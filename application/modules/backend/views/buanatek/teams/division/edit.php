<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/index') ?>">Buanatek</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/teams') ?>">Teams</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/teams/divisions') ?>">Team's Division</a></li>
			<li class="active breadcrumb-item" aria-current="page">Edit</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Teams &mdash; Division
        <div class="page-title-subheading">
          Edit Division of <strong><?= $division->name ?></strong>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open(current_url()) ?>
        <div class="card-header">
          Edit Category : <?= $division->name ?>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="name">*Name</label>
            <?= form_input($form['name']) ?>
            <?= form_error('name') ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?= form_hidden('id', $division->id) ?>
          <button type="submit" class="btn-wide btn-lg btn-square btn btn-success">Save</button>
          <?= anchor(site_url('backend/buanatek/teams/division_create'), 'add another', ['class' => 'btn btn-outline btn-lg']); ?>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
