<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/index') ?>">Buanatek</a></li>
			<li class="active breadcrumb-item" aria-current="page">Setting</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Buanatek &mdash; Setting
        <div class="page-title-subheading">
          Tarumanegara Buanatek Credential
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open_multipart(current_url()) ?>
        <div class="card-body">
          <div class="form-group">
            <label for="telp">Telp</label>
            <?= form_textarea($form['telp']) ?>
            <?= form_error('telp') ?>
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <?= form_textarea($form['address']) ?>
            <?= form_error('address') ?>
          </div>
          <?php if($logo): ?>
            <div class="form-group">
              <img src="<?php echo base_url('uploads/settings/'.$logo) ?>" alt="" class="img-thumbnail">
            </div>
          <?php endif;?>
          <div class="form-group">
            <label for="logo">Logo</label>
            <?= form_upload($form['logo']) ?>
            <?= form_error('logo') ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <button type="submit" class="btn-wide btn btn-lg btn-square btn-success">Save</a>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
