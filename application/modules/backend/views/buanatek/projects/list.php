<?php $i = 0; ?>
<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/index') ?>">Buanatek</a></li>
			<li class="active breadcrumb-item" aria-current="page">Project lists</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Buanatek &mdash; projects
        <div class="page-title-subheading">
          Project Lists
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
  <div class="card-header">
    <?= anchor('backend/buanatek/projects/create', 'add project', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
  </div>

  <div class="card-body">
    <table id="example" class="table table-hover table-striped table-borderes">
      <thead>
        <tr>
          <th>#</th>
          <th>Project Name</th>
          <th>project lists</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if($lists): ?>
          <?php foreach($lists as $key => $project): ?>
            <tr>
              <td><?= $key+1  ?></td>
              <td><?= $project->name ?></td>
              <td>
                <?php if(isset($project->projectlists) && count((array)($project->projectlists)) > 0 ): ?>
                  <?= anchor('backend/buanatek/projects/lists/'.$project->id, $project->projectlists->{$i}->counted_rows.' List(s)', ['class'=>'btn btn-square btn-info btn-sm']); ?>
                <?php else: ?>
                  <?= anchor('backend/buanatek/projects/lists/'.$project->id, 'Add List', ['class'=>'btn btn-square btn-info btn-sm']); ?>
                <?php endif; ?>
              </td>
              <td>
                <?= anchor('backend/buanatek/projects/edit/'.$project->id, 'Detail', ['class'=>'btn btn-square btn-success btn-sm']); ?>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>
          <tr class="text-center">
            <td colspan="3">no data</td>
          </tr>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>