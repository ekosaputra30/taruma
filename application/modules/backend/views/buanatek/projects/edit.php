<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/index') ?>">Buanatek</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/buanatek/projects') ?>">Projects</a></li>
			<li class="active breadcrumb-item" aria-current="page">Edit</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Project &mdash; Edit
        <div class="page-title-subheading">
          Edit Project : <strong><?= $project->name ?></strong>
        </div>
      </div>
    </div>
    <div class="page-title-actions">
      <a href="<?php echo site_url('backend/buanatek/projects/delete/'.$project->id) ?>" onclick="return confirm('are you sure?')" class="btn btn-shadow btn-danger btn-square btn-lg">
        <i class="lnr-trash" style="font-size: 1.65rem"></i>
      </a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open(current_url()) ?>
        <div class="card-header">
          Edit Project : <?= substr($project->name, 0, 10); ?>...
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="name">*Name</label>
            <?= form_input($form['name']) ?>
            <?= form_error('name') ?>
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <?= form_textarea($form['description']) ?>
            <?= form_error('description') ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?php echo form_hidden('id', $project->id);?>
          <button type="submit" class="btn-wide btn-lg btn-square btn btn-success">Save</a>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
