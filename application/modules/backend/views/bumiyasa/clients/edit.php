<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/clients') ?>">Clients</a></li>
			<li class="active breadcrumb-item" aria-current="page">Edit</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Clients &mdash; Edit Client
        <div class="page-title-subheading">
          Edit Client of <strong><?= $client->name ?></strong>
        </div>
      </div>
    </div>
    <div class="page-title-actions">
      <a href="<?php echo site_url('backend/bumiyasa/clients/delete/'.$client->id) ?>" onclick="return confirm('are you sure?')" class="btn btn-shadow btn-danger btn-square btn-lg">
        <i class="lnr-trash" style="font-size: 1.65rem"></i>
      </a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open_multipart(current_url()) ?>
        <div class="card-header">
          Edit Client : <?= $client->name ?>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="name">*Name</label>
            <?= form_input($form['name']) ?>
            <?= form_error('name') ?>
          </div>
          <div class="form-group">
            <label for="url">URL</label>
            <?= form_input($form['url']) ?>
            <?= form_error('url') ?>
          </div>
          <div class="form-group">
            <label for="category">*Category</label>
            <?= form_dropdown($form['category']['name'],$form['category']['dropdown'],$form['category']['value'],$form['category']['extra']) ?>
            <?= form_error('category') ?>
          </div>
          <?php if($client->image): ?>
            <div class="form-group">
              <img src="<?php echo base_url('uploads/clients/'.$client->image) ?>" alt="" class="img-thumbnail">
            </div>
          <?php endif;?>
          <div class="form-group">
            <label for="image">*Image</label>
            <?= form_upload($form['image']) ?>
            <?= form_error('image') ?>
            <?= $errorfileupload ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?= form_hidden('id', $client->id) ?>
          <button type="submit" class="btn-wide btn-lg btn-square btn btn-success">Save</button>
          <?= anchor(site_url('backend/bumiyasa/clients/create'), 'add another', ['class' => 'btn btn-outline btn-lg']); ?>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
