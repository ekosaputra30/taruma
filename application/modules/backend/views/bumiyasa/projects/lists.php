<?php $i = 0; ?>
<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="active breadcrumb-item" aria-current="page">Project lists</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Bumiyasa &mdash; Project lists
        <div class="page-title-subheading">
          Project Lists of <strong><?= $list->name ?></strong>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
  <div class="card-header">
    <?= anchor('backend/bumiyasa/projects/create_list/'.$id, 'new list', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
  </div>

  <div class="card-body">
    <table id="example" class="table table-hover table-striped table-borderes">
      <thead>
        <tr>
          <th>#</th>
          <th>Project Name</th>
          <th>Client</th>
          <th>Location</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if($lists): ?>
          <?php foreach($lists as $key => $project): ?>
            <tr>
              <td><?= $key+1  ?></td>
              <td><?= $project->name ?></td>
              <td><?= $project->client ?></td>
              <td><?= $project->location ?></td>
              <td>
                <?= anchor('backend/bumiyasa/projects/list_edit/'.$project->id, 'Detail', ['class'=>'btn btn-square btn-success btn-sm']); ?>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>
          <tr class="text-center">
            <td colspan="3">no data</td>
          </tr>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>