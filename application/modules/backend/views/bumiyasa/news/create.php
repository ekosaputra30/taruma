<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/news') ?>">News</a></li>
			<li class="active breadcrumb-item" aria-current="page">Create</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        News &mdash; Add Post
        <div class="page-title-subheading">
          Post News
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open_multipart(current_url()) ?>
        <div class="card-header">
          Add Post
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="title">*Title</label>
            <?= form_input($form['title']) ?>
            <?= form_error('title') ?>
          </div>
          <div class="form-group">
            <label for="image">*Image</label>
            <?= form_upload($form['image']) ?>
            <?= form_error('image') ?>
          </div>
          <div class="form-group">
            <label for="content">Content</label>
            <?= form_textarea($form['content']) ?>
            <?= form_error('content') ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <button type="submit" class="btn-wide btn-lg btn-square btn btn-success">Save</a>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
