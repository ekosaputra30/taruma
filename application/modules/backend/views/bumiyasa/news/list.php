<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="active breadcrumb-item" aria-current="page">News</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Bumiyasa &mdash; News
        <div class="page-title-subheading">
          News Lists
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
  <div class="card-header">
    <?= anchor('backend/bumiyasa/news/create', 'add news', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
  </div>

  <div class="card-body">
    <table id="example" class="table table-hover table-striped table-borderes">
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if($lists): ?>
          <?php foreach($lists as $key => $news): ?>
            <?php $i = 0; ?>
            <tr>
              <td><?= $key+1  ?></td>
              <td><?= $news->title ?></td>
              <td><?= $news->created_at ?></td>
              <td>
                <?= anchor('backend/bumiyasa/news/edit/'.$news->id, 'Detail', ['class'=>'btn btn-square btn-success btn-sm']); ?>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>
          <tr class="text-center">
            <td colspan="4">no data</td>
          </tr>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>