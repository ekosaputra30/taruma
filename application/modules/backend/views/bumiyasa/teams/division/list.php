<?php $i = 0 ?>
<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/teams') ?>">Teams</a></li>
			<li class="active breadcrumb-item" aria-current="page">Divisions</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Bumiyasa &mdash; Team's Division
        <div class="page-title-subheading">
          List of <strong>Team's Division</strong>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
  <div class="card-header">
    <?= anchor('backend/bumiyasa/teams/division_create', 'add division', ['class'=>'btn btn-warning btn-square btn-shadow btn-sm']); ?>
  </div>

  <div class="card-body">
    <table id="example" class="table table-hover table-striped table-borderes">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Teams</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if($lists): ?>
          <?php foreach($lists as $key => $division): ?>
            <tr>
              <td><?= $key+1  ?></td>
              <td><?= $division->name ?></td>
              <td>
                <?= anchor('backend/bumiyasa/teams/division_list/'.$division->id, ((!empty($division->teams)) ? $division->teams->$i->counted_rows : '0') .' Team(s)', ['class'=>'btn btn-square btn-info btn-sm']); ?>
              </td>
              <td>
                <?= anchor('backend/bumiyasa/teams/division_edit/'.$division->id, 'Detail', ['class'=>'btn btn-square btn-success btn-sm']); ?>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>
          <tr class="text-center">
            <td colspan="3">no data</td>
          </tr>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>