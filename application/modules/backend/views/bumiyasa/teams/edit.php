<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/teams') ?>">Teams</a></li>
			<li class="active breadcrumb-item" aria-current="page">Edit</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Teams &mdash; Edit
        <div class="page-title-subheading">
          Edit Team of <strong><?= $team->name ?></strong>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-6">
    <div class="main-card mb-3 card">
      <?php echo form_open_multipart(current_url()) ?>
        <div class="card-header">
          Edit Team : <?= $team->name ?>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="name">*Name</label>
            <?= form_input($form['name']) ?>
            <?= form_error('name') ?>
          </div>
          <div class="form-group">
            <label for="division">*Division</label>
            <?= form_dropdown($form['division']['name'],$form['division']['dropdown'],$form['division']['value'],$form['division']['extra']) ?>
            <?= form_error('division') ?>
          </div>
          <div class="form-group">
            <label for="position">*Position</label>
            <?= form_input($form['position']) ?>
            <?= form_error('position') ?>
          </div>
          <?php if($team->image): ?>
            <div class="form-group">
              <img src="<?php echo base_url('uploads/teams/'.$team->image) ?>" alt="" class="img-thumbnail">
            </div>
          <?php endif;?>
          <div class="form-group">
            <label for="image">*Image</label>
            <?= form_upload($form['image']) ?>
            <?= form_error('image') ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?php echo form_hidden('id', $team->id); ?>
          <button type="submit" class="btn-wide btn-lg btn-square btn btn-success">Save</button>
          <?= anchor(site_url('backend/bumiyasa/teams/create'), 'add another', ['class' => 'btn btn-outline btn-lg']); ?>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
