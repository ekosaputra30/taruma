<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="active breadcrumb-item" aria-current="page">Bumiyasa</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Dashboard
        <div class="page-title-subheading">
          Tarumanegara Bumiyasa Management
        </div>
      </div>
    </div>
    <div class="page-title-actions">
      <a href="<?php echo site_url('backend/bumiyasa/setting') ?>" class="btn btn-shadow btn-info btn-square btn-lg">
        <i class="lnr-cog" style="font-size: 1.65rem"></i>
      </a>
    </div>
  </div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
    <div class="grid-menu grid-menu-3col">
      <div class="no-gutters row">
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/services') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary"><i class="lnr-license btn-icon-wrapper"> </i>Services</a>
        </div>
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/projects') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-secondary"><i class="lnr-license btn-icon-wrapper"> </i>Project Lists</a>
        </div>
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/clients') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-success"><i class="lnr-license btn-icon-wrapper"> </i>Clients</a>
        </div>
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/teams') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-info"><i class="lnr-license btn-icon-wrapper"> </i>Team Support</a>
        </div>
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/news') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-danger"><i class="lnr-license btn-icon-wrapper"> </i>News</a>
        </div>
        <div class="col-sm-6 col-xl-4">
          <a href="<?= site_url('backend/bumiyasa/') ?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-alternate"><i class="lnr-license btn-icon-wrapper"> </i>Contact</a>
        </div>
      </div>
    </div>
  </div>
</div>
