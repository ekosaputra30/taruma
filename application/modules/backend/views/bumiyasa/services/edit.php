<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/services') ?>">Services</a></li>
			<li class="active breadcrumb-item" aria-current="page"><?= $service->name ?></li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Services &mdash; Edit
        <div class="page-title-subheading">
          Edit Service
        </div>
      </div>
    </div>
    <div class="page-title-actions">
      <a href="<?php echo site_url('backend/bumiyasa/services/delete/'.$id) ?>" onclick="return confirm('are you sure?')" class="btn btn-shadow btn-danger btn-square btn-lg">
        <i class="lnr-trash" style="font-size: 1.65rem"></i>
      </a>
    </div>
  </div>
</div>

<div class="row">

  <div class="col-12 col-lg-5">
    <?php echo form_open_multipart(current_url()) ?>
      <div class="main-card mb-3 card">
        <div class="card-header">
          Edit Service : <?= substr($service->name, 0, 10); ?>...
          <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm nav btn-group">
              <?php foreach(get_languages() as $keylang => $lang): ?>
                <a data-toggle="tab" href="#tab-eg1-<?php echo $keylang ?>" class="btn-shadow btn-square <?php echo ($lang->is_default == 1) ? 'active' : false ?> btn btn-primary"><?php echo $lang->code ?></a>
              <?php endforeach ?>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <?php foreach(get_languages() as $keylang => $lang): ?>
              <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : false ?>" id="tab-eg1-<?php echo $keylang ?>" role="tabpanel">
                <input type="hidden" name="id[<?= $lang->code?>]" value="<?= ($services->$keylang->lang == $lang->code) ? $services->$keylang->id : false ?>" >
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="name[<?= $lang->code?>]">*Name (<?= $lang->code ?>)</label>
                      <?= form_input($form[$lang->code]['name']) ?>
                      <?= form_error('name['.$lang->code.']') ?>
                    </div>
                    <div class="form-group">
                      <label for="description[<?= $lang->code?>]">*Description (<?= $lang->code ?>)</label>
                      <?= form_textarea($form[$lang->code]['description']) ?>
                      <?= form_error('description['.$lang->code.']') ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
            <div class="form-group">
              <label for="image">Image Cover</label>
              <input type="file" name="image" class="form-control" id="">
            </div>
          <?php if($service->images): ?>
            <div class="form-group">
              <img src="<?php echo base_url('uploads/services/cover/'.$service->images) ?>" alt="" class="img-thumbnail">
            </div>
          <?php endif;?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?php echo form_hidden('parent_id', $id);?>
          <button type="submit" class="btn-wide btn btn-success">Save</button>
        </div>
      </div>
    <?php echo form_close() ?>
  </div>
  <div class="col-12 col-lg-7">
    <div class="main-card mb-3 card" style="min-height: calc(100% - 2rem)">
      <div class="card-header">
        Sub Services
        <div class="btn-actions-pane-right">
          <div role="group" class="btn-group-sm nav btn-group">
            <a href="<?php echo site_url('backend/bumiyasa/services/create_sub/'.$id) ?>" class="btn btn-warning btn-sm btn-square">Add Sub Service</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-hover table-border">
          <thead>
            <tr>
              <th>No.</th>
              <th>Name of Sub Service</th>
              <th>Images</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
            
            <?php if(isset($service->servicesubs) > 0):?>
              <?php foreach($service->servicesubs as $key_sub => $servicesub): ?>
                <tr>
                  <td><?= $key_sub+1 ?></td>
                  <td><?= $servicesub->name ?></td>
                  <td><?= count(unserialize($servicesub->images)) ?> image(s)</td>
                  <td>
                    <a href="<?php echo site_url('backend/bumiyasa/services/edit_sub/'.$servicesub->parent_id) ?>" class="btn btn-info btn-sm btn-square">edit</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?> 
              <tr class="text-center">
                <td colspan="4">no sub service</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>