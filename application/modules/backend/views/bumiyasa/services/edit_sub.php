<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/services') ?>">Services</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/services/edit/'.$service->parent_id) ?>"><?= $service->name ?></a></li>
			<li class="active breadcrumb-item" aria-current="page"><?=$servicesub->name?></li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Sub Service &mdash; Edit
        <div class="page-title-subheading">
          Edit Sub Service <strong><?= $servicesub->name ?></strong>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-5">
    <?php echo form_open_multipart(current_url()) ?>
      <div class="main-card mb-3 card">
        <div class="card-header">
          <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm nav btn-group">
              <?php foreach(get_languages() as $keylang => $lang): ?>
                <a data-toggle="tab" href="#tab-eg1-<?php echo $keylang ?>" class="btn-shadow btn-square <?php echo ($lang->is_default == 1) ? 'active' : false ?> btn btn-primary"><?php echo $lang->code ?></a>
              <?php endforeach ?>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <?php foreach(get_languages() as $keylang => $lang): ?>
              <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : false ?>" id="tab-eg1-<?php echo $keylang ?>" role="tabpanel">
                <input type="hidden" name="id[<?= $lang->code?>]" value="<?= ($servicesubs->$keylang->lang == $lang->code) ? $servicesubs->$keylang->id : false ?>" >
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="name[<?= $lang->code?>]">*Name (<?= $lang->code ?>)</label>
                      <?= form_input($form[$lang->code]['name']) ?>
                      <?= form_error('name['.$lang->code.']') ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <?php echo form_hidden('parent_id', $servicesub->parent_id);?>
          <button type="submit" class="btn-wide btn btn-success">Save</button>
        </div>
      </div>
    <?php echo form_close() ?>
  </div>
  <div class="col-12 col-lg-7">
    <div class="main-card mb-3 card" style="height: calc(100% - 2rem)">
      <div class="card-header">
        Upload New Image
      </div>
      <div class="card-body">
        <div id="fine-uploader-manual-trigger" class="mb-3" style="height: 100%"></div>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="main-card mb-3 card">
      <div class="card-header">
        Image Gallery
        <?php if($servicesub->images != NULL): ?>
          <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm nav btn-group">
              <a href="<?php echo site_url('backend/bumiyasa/services/delete_galleries/'.$id) ?>" class="btn btn-danger btn-sm btn-square">Remove All Images</a>
            </div>
          </div>
        <?php endif ?>
      </div>
      <div class="card-body">
        <?php if(count($galleries) > 0):?>
          <div class="row">
            <?php foreach($galleries as $gallery):?>
              <div class="col-6 col-lg-4 mb-4">
                <div class="card">
                  <img src="<?php echo base_url('uploads/services/'.$gallery['name']) ?>" class="card-img-top" alt="">
                  <div class="card-body">
                    <p><strong>name :</strong> <?= $gallery['name'] ?></p>
                    <a href="<?php echo site_url('backend/bumiyasa/services/remove_image/'.$id.'/'.$gallery['uuid']) ?>" class="btn btn-danger btn-square btn-shadow btn-sm">Remove</a>
                  </div>
                </div>
              </div>
            <?php endforeach;?> 
          </div>
        <?php else: 'gallery is empty'; endif; ?>
      </div>
    </div>
  </div>
</div>
