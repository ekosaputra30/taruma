<div class="app-page-title bg-white">
  <nav class="" aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent pl-0">
      <li class="breadcrumb-item"><a href="<?php echo site_url('backend') ?>">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/index') ?>">Bumiyasa</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('backend/bumiyasa/services') ?>">Services</a></li>
			<li class="active breadcrumb-item" aria-current="page">Create</li>
		</ol>
	</nav>
  <div class="page-title-wrapper">
    <div class="page-title-heading">
      <div class="page-title-icon">
        <div class="bg-premium-dark icon-gradient lnr-apartment"></div>
      </div>
      <div>
        Services &mdash; Create
        <div class="page-title-subheading">
          Add New Service
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-lg-5">
    <div class="main-card mb-3 card">
      <?php echo form_open_multipart(current_url()) ?>
        <div class="card-header">
          Add Service
          <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm nav btn-group">
              <?php foreach(get_languages() as $keylang => $lang): ?>
                <a data-toggle="tab" href="#tab-eg1-<?php echo $keylang ?>" class="btn-shadow btn-square <?php echo ($lang->is_default == 1) ? 'active' : false ?> btn btn-primary"><?php echo $lang->code ?></a>
              <?php endforeach ?>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <?php foreach(get_languages() as $keylang => $lang): ?>
              <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : false ?>" id="tab-eg1-<?php echo $keylang ?>" role="tabpanel">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="name[<?= $lang->code?>]">*Name (<?= $lang->code ?>)</label>
                      <?= form_input($form[$lang->code]['name']) ?>
                      <?= form_error('name['.$lang->code.']') ?>
                    </div>
                    <div class="form-group">
                      <label for="description[<?= $lang->code?>]">*Description (<?= $lang->code ?>)</label>
                      <?= form_textarea($form[$lang->code]['description']) ?>
                      <?= form_error('description['.$lang->code.']') ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
        </div>
        <div class="d-block card-footer">
          <button type="submit" class="btn-wide btn btn-success">Save</a>
        </div>
      <?php echo form_close() ?>
    </div>
  </div>
</div>
